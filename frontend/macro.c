#include "frontend/macro.h"
#include "frontend/error.h"
#include "frontend/frontend.h"
#include "util/array.h"
#include "util/hash.h"
#include "util/xmalloc.h"
#include <string.h>

// copy location nexts off the arena
static struct ast_location copy_loc(struct ast_location loc)
{
	if (loc.next) {
		struct ast_location next = copy_loc(loc.next);
		loc.next = xmalloc(sizeof(struct ast_location));
		*loc.next = next;
	}
	return loc;
}

static bool tok_eq(struct lex_token a, struct lex_token b)
{
	if (a.kind == LEX_SYMBOL) return true;
	if (a.kind != b.kind) return false;
	switch (a.kind) {
	case LEX_EOF:
		return false;
	case LEX_ALPHAOP:
		return a.symbol == b.symbol;
	case LEX_STRING:
		return strcmp(a.string, b.string) == 0;
	case LEX_INTEGER:
		return a.integer == b.integer;
	default: // no data to check
		return true;
	}
}

static void splice_loc(struct ast_location *list, struct ast_location loc, struct mem_arena *arena)
{
	struct ast_location *added = NULL; // copy of location on arena
	struct ast_location *add; // tail of added
	for (struct ast_location *cur = &loc; cur; cur = cur->next) {
		struct mem_arena *next = mem_arena_alloc(arena, 1, struct ast_location);
		*next = *cur;
		if (!added) {
			added = add = next;
		} else {
			add->next = next;
			add = next;
		}
	}
	add->next = list->next;
	list->next = add;
}

static void expand(uint32_t bindc, uint32_t bound[bindc], size_t bind_len[bindc], struct lex_token *bound_to[bindc],
		uint32_t len, struct lex_token expand[len], struct lex_state *out,
		struct ast_location loc, struct mem_arena *arena)
{
	for (uint32_t i = 0; i < len; i++) {
		if (expand[i].kind == LEX_HASH) {
			log_assert(i+1 < len); // make sure this check is done before expansion for better errors
			i++;
			if (expand[i].kind == LEX_SYMBOL) {
				for (uint32_t j = 0; j < bindc; j++) {
					if (expand[i].symbol == bound[j]) {
						for (size_t k = 0; k < bind_len[j]; k++) {
							struct lex_token tok = bound_to[j];
							splice_loc(tok.loc, loc, arena);
							unlex(out, bound_to[j] + k);
						}
						break;
					}
				}
				// make sure to check we found the symbol before expansion
			} else {
				log_todo("Macro CTFE");
			}
		} else {
			struct lex_token tok = expand[i];
			struct ast_location *loc = mem_arena_alloc(arena, 1, struct ast_location);
			*loc = *tok.loc;
			splice_loc(loc, loc, arena);
			tok.loc = loc;
			unlex(out, &tok);
		}
	}
}

struct dispatch_table {
	struct ast_location loc; // location of *defined* macro
	struct lex_token *next_tok;
	struct dispatch_table *next_table;
	struct lex_token *this; // 1 if valid empty macro
	uint32_t next_len;
	uint32_t next_back;
	uint32_t this_len;
	bool varadic;
};

static struct hash_table dispatch_table = {
	.keysize = sizeof(uint32_t),
	.valsize = sizeof(struct dispatch_table *)
};

// we need to iterate through the looked up dispatch tables to bind stuff
// so we do an iterator like thing
// this is the state of the iteration
struct dispatch_lookup {
	// the stuff below here is private
	uint32_t name;
	uint32_t argc;
	struct lex_token *args;
	struct ast_location loc;
	struct dispatch_table r;
	uint32_t i;
}

// set up the dispatch lookup state for iteration
static struct dispatch_lookup dispatch_lookup_init(uint32_t name, uint32_t argc, struct lex_token *args, struct ast_location loc)
{
	struct dispatch_lookup r = {
		.name = name,
		.argc = argc,
		.args = args,
		.loc = loc,
		.i = 0
	};
	if (!hash_table_get(dispatch_table, &name, &r.r)) {
		error(loc, "Attempt to use undefined macro %s", fe_symbols[name]);
	}
}

// get the next dispatch table (in s or owned by the big table) or NULL if no more tables to look through
// m is the matched token in the dispatch table
static struct dispatch_lookup_result { struct dispatch_table *t; struct lex_token *m } dispatch_lookup(struct dispatch_lookup *s)
{
	if (s->i < s->argc) {
		for (j = 0; j < s->r.next_len; j++) {
			if (tok_eq(s->r.next_tok[j], s->args[i])) {
				struct dispatch_table *t = &s->r;
				struct lex_token *m = s->r.next_tok+j;
				s->r = s->r.next_table[j];
				s->i++;
				return (struct dispatch_lookup_result) {
					.t = t,
					.m = m
				};
			}
		}
		error(s->loc, "Bad number or use of arguments for macro %s", fe_symbols[name]);
	} else if (s->i == s->argc) {
		error_assert(s->r.this, s->loc, "Too few arguments to macro %s", fe_symbols[s->name]);
		s->i++;
		return (struct dispatch_lookup_result) {
			.t = &s.r,
			.m = NULL
		};
	} else {
		return (struct dispatch_lookup_result) {};
	}
}

static void dispatch_insert(uint32_t name, uint32_t argc, struct lex_token args[argc],
		uint32_t len, struct lex_token expand[len], bool varadic, struct ast_location loc)
{
	struct dispatch_table *t;
	if (!hash_table_get(dispatch_table, name, &t)) {
		t = xmalloc(sizeof(struct dispatch_table));
		*t = (struct dispatch_table) {};
		hash_table_set(&dispatch_table, name, &t);
	}

	while (argc > 0) {
		error_assert(args[0].kind != LEX_HASH, loc,
				"Can't use %s as macro argument", "HASH");
		for (uint32_t i = 0; i < t->next_len; i++) {
			if (tok_eq(t->next_tok[i], args[0])) {
				t = t->next_table + i;
				goto found;
			}
		}
		struct dispatch_table t1 = {};
		if (t->next_len >= t->next_back) {
			if (t->next_back) {
				t->next_back *= 2;
			} else {
				t->next_back = 2;
			}
			t->next_tok = xrealloc(t->next_tok, t->next_back * sizeof(struct lex_token));
			t->next_table = xrealloc(t->next_table, t->next_back * sizeof(struct lex_token));
		}
		t->next_tok[t->next_len] = args[0];
		t->next_table[t->next_len] = (struct dispatch_table) {};
		t->next_len++;
		t = t->next_table + t->next_len - 1;
found:
		argc--;
		args++;
	}

	error_assert(!t->this, loc,
			"Redefinition of macro %s", fe_symbols[name]);
	t->this_len = len;
	t->this = len ? xmalloc(sizeof(expand)) : 1;
	memcpy(t->this, expand, sizeof(expand));
	t->varadic = varadic;
	t->loc = copy_loc(loc);
}

void macro_expand(uint32_t sym, uint32_t argc, uint32_t arg_len[argc], struct lex_token *args[argc],
		struct ast_location loc, struct lex_state *out, struct mem_arena *arena)
{
	log_assert(argc < 64); // we'll make VLAs of this size, and don't want unchecked stack overflow

	struct lex_token lookup_args[argc];
	for (uint32_t i = 0; i < argc; i++) {
		if (arg_len[i] == 1) {
			lookup_args[i] = args[i][0];
		} else {
			lookuup_args[i] = (struct lex_token) { .kind = LEX_EOF };
		}
	}

	uint32_t var_argc = argc;
	uint32_t bindc = 0;
	uint32_t bind_len[argc];
	struct lex_token bound[argc];
	struct lex_token bind[argc];

	struct dispatch_lookup lookup_state = dispatch_lookup_init(sym, argc, lookup_args, loc);
	auto lookup_result = dispatch_lookup(&lookup_state);
	log_assert(lookup_result.t);
	uint32_t i = 0; // current index of looked at argument
	while (lookup_result.t) {
		if (lookup_result.m && lookup_result.m->kind == LEX_SYMBOL) {
			bind_len[bindc] = arg_len[i];
			bound[bindc] = 
