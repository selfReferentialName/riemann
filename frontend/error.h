#ifndef FRONTEND_ERROR_H
#define FRONTEND_ERROR_H

#include "frontend/ast.h"
#include <stdarg.h>

/// Is this an error? warning? etc.
enum error_kind {
	ERROR, //< Fatal error
	WARNING, //< Non-fatal warning
};

// Do nothing, used for macros
[[maybe_unused]] static void err_do_nothing(void) {};

/// Write a diagnostic
void err(enum error_kind kind, struct ast_location loc, const char *fmt, const char *file, size_t line, ...);

/// Throw an error
#define error(loc, fmt, ...) err(ERROR, loc, fmt, __FILE__, __LINE__, __VA_ARGS__)

/// Throw an error if an assertion fails
#define error_assert(cond, loc, fmt, ...) ((cond) ? err_do_nothing() : error(loc, fmt, __VA_ARGS__))

/// Report a warning
#define warn(loc, fmt, ...) err(WARNING, loc, fmt, __FILE__, __LINE__, __VA_ARGS__)

#endif
