#ifndef FRONTEND_LEXER_H
#define FRONTEND_LEXER_H

#include "util/arena.h"
#include <stddef.h>
#include <stdint.h>

/// The state the lexer has to know where it is and such.
struct lex_state {
	size_t i; //< The offset where the lexer is looking at
	const char *file; //< The file we're currently in
	struct lex_token *next; //< The next few tokens to return first (from lookahead)
	uint32_t next_count; //< The number of tokens in lookahead
	uint32_t next_back; //< Backing size of next
	uint32_t line; //< The line we're currently on
	uint32_t col; //< The column we're currently on
};

/// The various kinds of tokens
/// Guarenteed to be ordered with all operations before all keywords
enum lex_token_kind {
	LEX_EOF, //< Nothing more to lex
	LEX_NEWLINE, //< A syntactically significant newline
	LEX_COMMA, //< A syntactically significant comma
	LEX_HASH, //< #, used for macro stuff
	LEX_RLB, //< (, a round left bracket
	LEX_RRB, //< ), a round right bracket
	LEX_EQSIGN, //< =, the assignment operator
	LEX_DBLCOLON, //< ::, used for declaration
	LEX_DBLSTAR, //< **, the exponentiation operator
	LEX_STAR, //< *, the multiplication operator
	LEX_SLASH, //< /, the right division operator
	LEX_BACKSLASH, //< \, the left division operator
	LEX_PLUS, //< +, the addition operator
	LEX_MINUS, //< -, the negation and subtraction operator
	LEX_COLON, //< :, a single colon
	LEX_DBLEQ, //< ==, the equality operator
	LEX_NEQ, //< =/= and =\=, the inequality operator
	LEX_LT, //< <, the less than operator
	LEX_LE, //< <=, the less than or equal to operator
	LEX_GE, //< >=, the greater than or equal to operator
	LEX_GT, //< >, the greater than operator
	LEX_ALPHAOP, //< An alphanumeric operator
	LEX_SYMBOL, //< An identifier
	LEX_STRING, //< A string litteral
	LEX_INTEGER, //< An integer litteral
	LEX_END, //< The end keyword
	LEX_PROGRAM, //< The program keyword
	LEX_OUTPUT, //< The output keyword
	LEX_TRUE, //< The true keyword
	LEX_FALSE, //< The false keyword
	LEX_FUNCTION, //< The function keyword
	LEX_SUBROUTINE, //< The subroutine keyword
	LEX_RETURN, //< The return keyword
	LEX_NOT, //< The not keyword
	LEX_AND, //< The and keyword
	LEX_OR, //< The or keyword
	LEX_WHILE, //< The while keyword
	LEX_ELSE, //< The else keyword
	LEX_IF, //< The if keyword
};

/// Token kind to string description of token kind
extern const char *lex_token_name[LEX_IF+1];

/// A token with its kind.
struct lex_token {
	const char *file; //< File the token is in
	uint32_t line, col; //< Position information at start
	union {
		uint32_t symbol; //< The ID of an identifier
		char *string; //< The string of a litteral
		int64_t integer; //< The integer of a litteral
	};
	enum lex_token_kind kind; //< The kind of token
};

/// Get the next token.
struct lex_token lex(struct lex_state *state);

/// Put a token onto the lexed stuff
void unlex(struct lex_state *state, const struct lex_token *tok);

#endif
