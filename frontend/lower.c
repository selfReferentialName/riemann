#include "frontend/lower.h"
#include "frontend/frontend.h"
#include "frontend/error.h"
#include "core/ir.h"
#include "util/hash.h"
#include "util/arena.h"
#include "util/array.h"
#include "util/xmalloc.h"
#include "util/log.h"
#include <string.h>
#include <stdio.h>

static struct hash_table local_table = {
	.keysize = sizeof(uint32_t),
	.valsize = sizeof(uint32_t)
};

static struct hash_table local_type_table = {
	.keysize = sizeof(uint32_t),
	.valsize = sizeof(struct ir_type)
};

// get IR variable pointer to variable
static uint32_t lookup(uint32_t var)
{
	uint32_t r;
	if (hash_table_get(local_table, &var, &r)) {
		return r;
	} else {
		log_f(LOG_ERROR, "Attempt to lower undeclared variable %u (%s)", var, fe_symbols[var]);
		log_unreachable();
	}
}

// get IR variable type
static struct ir_type lookup_type(uint32_t var)
{
	struct ir_type r;
	if (hash_table_get(local_type_table, &var, &r)) {
		return r;
	} else {
		log_unreachable();
	}
}

// convert function/subroutine id to IR function id
static uint32_t *fun_to_fun;
static size_t fun_to_fun_back;
static uint32_t *sub_to_fun;
static size_t sub_to_fun_back;
static uint32_t *fun_op_to_fun;
static size_t fun_op_to_fun_back;
static uint32_t *fun_alphaop_to_fun;
static size_t fun_alphaop_to_fun_back;

static struct ir_type lower_type(struct ast_type type, struct mem_arena *arena)
{
	switch (type.kind) {
	case TAST_TEXT:
		return (struct ir_type) {
			.kind = IR_TEXT
		};
	case TAST_INTEGER:
		return (struct ir_type) {
			.kind = IR_INT
		};
	case TAST_LOGIC:
		return (struct ir_type) {
			.kind = IR_LOGIC
		};
	default:
		log_unreachable();
	}
}

// program name to symbol name
static char *mangle_program(const char *name)
{
	size_t len = strlen(name) + 3;
	char *r = xmalloc(len);
	r[0] = '_';
	r[1] = 'p';
	strcpy(r+2, name);
	r[len - 1] = '\0';
	return r;
}

static const char op_name[AST_OR+1][8] = {
	"pow",
	"times",
	"rdiv",
	"ldiv",
	"add",
	"sub",
	"eq",
	"neq",
	"lt",
	"le",
	"ge",
	"gt",
	"and",
	"or",
};

// procedure name to symbol name
static char *mangle_proc(struct ast_top what)
{
	size_t len = strlen(fe_symbols[what.proc_name]) + 16;
	char *r = xmalloc(len);
	log_assert(snprintf(r, len, "_%c%u_%s",
				what.kind==AST_FUNCTION ? 'f' : what.kind==AST_SUBROUTINE ? 's' : what.kind==AST_OP_FUNCTION ? 'o' : 'a',
				what.proc_id, what.kind==AST_OP_FUNCTION ? op_name[what.proc_op] : fe_symbols[what.proc_name])
			< len);
	return r;
}

// the state of how to add functions and select variables
// bb is mutable, so you may not be bulding in the block you started building in
struct builder {
	struct ir_fun *fun; // what function we're building in
	uint32_t bb; // index of the current basic block
};

static void bb_insert(struct ir_bb *bb, struct ir_inst *inst)
{
	array_insert32((void **) &bb->insts, &bb->inst_count, &bb->inst_back, inst, sizeof(struct ir_inst));
}

static void insert(struct builder *b, struct ir_inst *inst)
{
	bb_insert(&b->fun->bb[b->bb], inst);
}

static uint32_t lower_expr(struct ast_expr expr, struct builder *b, uint32_t *next_var, struct mem_arena *arena);
// defaults terminator to IR_RET
// overwrites block pointed to by bld
static void lower_block(struct ast_stmnt *stmnts, uint32_t *next_var, uint32_t len, struct mem_arena *arena, struct builder *bld);

static uint32_t fun_insert(struct ir_fun *fun, struct ir_bb *bb)
{
	array_insert32((void **) &fun->bb, &fun->bb_count, &fun->bb_back, bb, sizeof(struct ir_bb));
	return fun->bb_count - 1;
}

static uint32_t fun_newbb(struct ir_fun *fun)
{
	uint32_t r = fun->bb_count;
	if (r >= fun->bb_back) {
		fun->bb_back *= 2;
		fun->bb = xrealloc(fun->bb, fun->bb_back*sizeof(struct ir_bb));
	}
	fun->bb_count++;
	return r;
}

static uint32_t lower_builtin_op(struct ast_expr op, struct builder *b, uint32_t *next_var, struct mem_arena *arena)
{
	static const enum ir_op lut_int[] = {
		[AST_TIMES] = IR_MUL,
		[AST_RDIV] = IR_DIV,
		[AST_ADD] = IR_ADD,
		[AST_SUB] = IR_SUB,
		[AST_EQ] = IR_EQ,
		[AST_NEQ] = IR_NE,
		[AST_LT] = IR_LT,
		[AST_LE] = IR_LE,
		[AST_GE] = IR_GE,
		[AST_GT] = IR_GT
	};
	struct ir_inst inst;

	if (op.lhs->type->kind == TAST_INTEGER && op.kind == AST_OP) {
		uint32_t lhs = lower_expr(*op.lhs, b, next_var, arena);
		uint32_t rhs = lower_expr(*op.rhs, b, next_var, arena);
		switch (op.op) {
		case AST_LDIV:
			inst = (struct ir_inst) {
				.kind = IR_OP,
				.type = lower_type(*op.type, arena),
				.out = *next_var,
				.op = IR_DIV,
				.lhs = rhs,
				.rhs = lhs
			};
			insert(b, &inst);
			(*next_var)++;
			return inst.out;
		default:
			inst = (struct ir_inst) {
				.kind = IR_OP,
				.type = lower_type(*op.type, arena),
				.out = *next_var,
				.op = lut_int[op.op],
				.lhs = lhs,
				.rhs = rhs
			};
			insert(b, &inst);
			(*next_var)++;
			return inst.out;
		}
	} else if (op.lhs->type->kind == TAST_INTEGER && op.kind == AST_ALPHAOP) {
		uint32_t lhs = lower_expr(*op.lhs, b, next_var, arena);
		uint32_t rhs = lower_expr(*op.rhs, b, next_var, arena);
		inst = (struct ir_inst) {
			.kind = IR_OP,
			.type = lower_type(*op.type, arena),
			.out = *next_var,
			.lhs = lhs,
			.rhs = rhs
		};
		if (op.alphaop == fe_sym_mod) {
			inst.op = IR_MOD;
		} else {
			log_unreachable();
		}
		insert(b, &inst);
		(*next_var)++;
		return inst.out;
	} else if (op.lhs->type->kind == TAST_LOGIC && op.kind == AST_OP) {
		uint32_t lhs = lower_expr(*op.lhs, b, next_var, arena);
		switch (op.op) {

		case AST_AND:
		case AST_OR:
			{
			uint32_t maybe = fun_newbb(b->fun);
			b->fun->bb[maybe] = (struct ir_bb) { // block to evaluate RHS
				.inst_back = 4
			};
			b->fun->bb[maybe].insts = xmalloc(sizeof(struct ir_inst) * b->fun->bb[maybe].inst_back);

			struct ir_bb fallthrough_copied = { // block which knows result
				.inst_back = 16,
				.args = xmalloc(sizeof(uint32_t)),
				.arg_types = xmalloc(sizeof(uint32_t)),
				.arg_count = 1
			};
			uint32_t r = fallthrough_copied.args[0] = *next_var; // result (block argument)
			(*next_var)++;
			fallthrough_copied.arg_types[0] = (struct ir_type) { .kind = IR_LOGIC };
			fallthrough_copied.insts = xmalloc(sizeof(struct ir_inst) * b->fun->bb[maybe].inst_back);
			uint32_t fallthrough = fun_insert(b->fun, &fallthrough_copied);

			struct builder bm = *b; // evaluate RHS in maybe
			bm.bb = maybe;
			uint32_t rhs = lower_expr(*op.rhs, &bm, next_var, arena);
			b->fun->bb[maybe].term = (struct ir_term) {
				.kind = IR_GOTO,
				.args = xmalloc(sizeof(uint32_t)),
				.arg_count = 1,
				.target = fallthrough
			};
			b->fun->bb[maybe].term.args[0] = rhs;

			if (op.op == AST_AND) { // branch if we need to
				b->fun->bb[b->bb].term = (struct ir_term) {
					.kind = IR_BRANCH,
					.br_cond = lhs,
					.br_then = maybe,
					.br_else = fallthrough,
					.br_else_args = xmalloc(sizeof(uint32_t)),
					.br_else_argc = 1
				};
				b->fun->bb[b->bb].term.br_else_args[0] = lhs; // convinient false
			} else {
				b->fun->bb[b->bb].term = (struct ir_term) {
					.kind = IR_BRANCH,
					.br_cond = lhs,
					.br_then = fallthrough,
					.br_then_args = xmalloc(sizeof(uint32_t)),
					.br_then_argc = 1,
					.br_else = maybe,
				};
				b->fun->bb[b->bb].term.br_then_args[0] = lhs; // convinient true
			}
			b->bb = fallthrough;

			return r;
			}

		default:
			{
			uint32_t rhs = lower_expr(*op.rhs, b, next_var, arena);
			inst = (struct ir_inst) {
				.kind = IR_OP,
				.type = lower_type(*op.type, arena),
				.out = *next_var,
				.op = lut_int[op.op],
				.lhs = lhs,
				.rhs = rhs
			};
			insert(b, &inst);
			(*next_var)++;
			return inst.out;
			}
		}

	} else {
		log_unreachable();
	}
}


static uint32_t lower_expr(struct ast_expr expr, struct builder *b, uint32_t *next_var, struct mem_arena *arena)
{
	struct ir_inst inst;
	switch (expr.kind) {
	case AST_STRING:
		inst = (struct ir_inst) {
			.kind = IR_CONST_STRING,
			.out = *next_var,
			.type = { .kind = IR_TEXT },
			.string = expr.string,
			.string_len = strlen(expr.string) + 1
		};
		insert(b, &inst);
		(*next_var)++;
		return inst.out;
	case AST_INTEGER:
		inst = (struct ir_inst) {
			.kind = IR_CONST_INT,
			.out = *next_var,
			.type = { .kind = IR_INT },
			.intval = expr.integer
		};
		insert(b, &inst);
		(*next_var)++;
		return inst.out;
	case AST_LOGIC:
		inst = (struct ir_inst) {
			.kind = IR_CONST_LOGIC,
			.out = *next_var,
			.type = { .kind = IR_LOGIC },
			.logicval = expr.logic
		};
		insert(b, &inst);
		(*next_var)++;
		return inst.out;
	case AST_VARIABLE:
		inst = (struct ir_inst) {
			.kind = IR_LOAD,
			.out = *next_var,
			.type = lookup_type(expr.var),
			.arg = lookup(expr.var)
		};
		insert(b, &inst);
		(*next_var)++;
		return inst.out;
	case AST_FCALL:
		log_assert(expr.argc >= 1);
		log_assert(expr.args[0].kind == AST_FUNUSE); // TODO: higher order functions
		inst = (struct ir_inst) {
			.kind = IR_CALL,
			.out = 0, // set later
			.type = lower_type(*expr.type, arena),
			.call_fun = fun_to_fun[expr.args[0].id],
			.call_args = xmalloc((expr.argc-1) * sizeof(uint32_t)),
			.call_arg_count = expr.argc - 1
		};
		for (uint32_t i = 0; i < expr.argc - 1; i++) {
			inst.call_args[i] = lower_expr(expr.args[i+1], b, next_var, arena);
		}
		inst.out = *next_var;
		insert(b, &inst);
		(*next_var)++;
		return inst.out;
	case AST_OP:
	case AST_ALPHAOP:
		if (expr.op_id == 0) {
			return lower_builtin_op(expr, b, next_var, arena);
		}
		inst = (struct ir_inst) {
			.kind = IR_CALL,
			.out = 0,
			.type = lower_type(*expr.type, arena),
			.call_fun = (expr.kind==AST_OP?fun_op_to_fun:fun_alphaop_to_fun)[expr.op_id],
			.call_args = xmalloc(2 * sizeof(uint32_t)),
			.call_arg_count = 2
		};
		inst.call_args[0] = lower_expr(*expr.lhs, b, next_var, arena);
		inst.call_args[1] = lower_expr(*expr.rhs, b, next_var, arena);
		inst.out = *next_var;
		insert(b, &inst);
		(*next_var)++;
		return inst.out;
	case AST_NOT:
	case AST_NEG:
		inst = (struct ir_inst) {
			.kind = expr.kind==AST_NOT ? IR_NOT : IR_NEG,
			.type = lower_type(*expr.type, arena),
			.arg = lower_expr(*expr.arg, b, next_var, arena)
		};
		inst.out = *next_var;
		insert(b, &inst);
		(*next_var)++;
		return inst.out;
	default:
		log_unreachable();
	}
}

static void lower_if(struct ast_stmnt *stmnts, uint32_t *next_var, uint32_t len, struct mem_arena *arena, struct builder *bld)
{
	uint32_t cond = lower_expr(*stmnts[0].if_cond, bld, next_var, arena);
	uint32_t bb0 = bld->bb;

	uint32_t t_h = bld->bb = fun_newbb(bld->fun);
	lower_block(stmnts[0].stmnts, next_var, stmnts[0].block_len, arena, bld);
	uint32_t t = bld->bb;

	uint32_t e = 0, e_h;
	if (stmnts[0].if_else) {
		e_h = bld->bb = fun_newbb(bld->fun);
		lower_block(stmnts[0].if_else, next_var, stmnts[0].if_else_len, arena, bld);
		e = bld->bb;
	}

	uint32_t f = bld->bb = fun_newbb(bld->fun);
	lower_block(stmnts+1, next_var, len-1, arena, bld);

	bld->fun->bb[bb0].term = (struct ir_term) {
		.kind = IR_BRANCH,
		.br_cond = cond,
		.br_then = t_h,
		.br_else = e ? e_h : f
	};
	bld->fun->bb[t].term = (struct ir_term) {
		.kind = IR_GOTO,
		.target = f
	};
	if (e) bld->fun->bb[e].term = (struct ir_term) {
		.kind = IR_GOTO,
		.target = f
	};
}

static void lower_while(struct ast_stmnt *stmnts, uint32_t *next_var, uint32_t len, struct mem_arena *arena, struct builder *bld)
{
	uint32_t entry_cond = lower_expr(*stmnts[0].while_cond, bld, next_var, arena);
	uint32_t bb0 = bld->bb;

	uint32_t body = bld->bb = fun_newbb(bld->fun);
	lower_block(stmnts[0].stmnts, next_var, stmnts[0].block_len, arena, bld);
	uint32_t body_cond = lower_expr(*stmnts[0].while_cond, bld, next_var, arena);
	uint32_t body_end = bld->bb;

	uint32_t rest = bld->bb = fun_newbb(bld->fun);
	lower_block(stmnts+1, next_var, len-1, arena, bld);

	bld->fun->bb[bb0].term = (struct ir_term) {
		.kind = IR_BRANCH,
		.br_cond = entry_cond,
		.br_then = body,
		.br_else = rest
	};
	bld->fun->bb[body_end].term = (struct ir_term) {
		.kind = IR_BRANCH,
		.br_cond = body_cond,
		.br_then = body,
		.br_else = rest
	};
}


static void lower_stmnt(struct ast_stmnt stmnt, struct builder *b, uint32_t *next_var, struct mem_arena *arena)
{
	switch (stmnt.kind) {
	case AST_DECLARATION:
		{
			uint32_t p = *next_var;
			(*next_var)++;
			hash_table_set(&local_table, &stmnt.decl_name, &p);
			struct ir_inst decl_inst = {
				.kind = IR_DECLARE,
				.out = p,
				.type = { .kind = IR_LINPTR, .arg = xmalloc(sizeof(struct ir_type)) }
			};
			*decl_inst.type.arg = lower_type(stmnt.decl_type, arena);
			hash_table_set(&local_type_table, &stmnt.decl_name, decl_inst.type.arg);
			insert(b, &decl_inst);
			if (stmnt.decl_init) {
				struct ir_inst set_inst = {
					.kind = IR_STORE,
					.argn = {
						p,
						lower_expr(*stmnt.decl_init, b, next_var, arena)
					}
				};
				insert(b, &set_inst);
			}
			return;
		}
	case AST_ASSIGNMENT:
		{
			uint32_t p = lookup(stmnt.set_name);
			struct ir_inst inst = {
				.kind = IR_STORE,
				.argn = {
					p,
					lower_expr(*stmnt.set_value, b, next_var, arena)
				}
			};
			insert(b, &inst);
			return;
		}
	case AST_SCALL:
		{
			struct ir_inst inst = {
				.kind = IR_CALL,
				.call_fun = sub_to_fun[stmnt.sub_id],
				.call_args = xmalloc(stmnt.call_len * sizeof(uint32_t)),
				.call_arg_count = stmnt.call_len
			};
			for (uint32_t i = 0; i < stmnt.call_len; i++) {
				inst.call_args[i] = lower_expr(stmnt.call_args[i], b, next_var, arena);
			}
			insert(b, &inst);
			return;
		}
	case AST_OUTPUT:
		{
			uint32_t *args = xmalloc((stmnt.io_len+1) * sizeof(uint32_t));
			if (stmnt.format) {
				args[0] = lower_expr(*stmnt.format, b, next_var, arena);
			} else {
				struct ir_inst fmt_inst = {
					.kind = IR_CONST_STRING,
					.out = *next_var,
					.string = xmalloc(stmnt.io_len*3 + 1),
					.string_len = stmnt.io_len*3 + 1
				};
				insert(b, &fmt_inst);
				(*next_var)++;
				for (uint32_t i = 0; i < stmnt.io_len; i++) {
					fmt_inst.string[3*i] = '%';
					switch (stmnt.io_args[i].type->kind) {
					case TAST_TEXT:
						fmt_inst.string[3*i + 1] = 's';
						break;
					case TAST_INTEGER:
						fmt_inst.string[3*i + 1] = 'd';
						break;
					case TAST_LOGIC:
						fmt_inst.string[3*i + 1] = 'l';
						break;
					default:
						log_unreachable();
					}
					fmt_inst.string[3*i + 2] = ' ';
				}
				fmt_inst.string[3*stmnt.io_len - 1] = '\n';
				fmt_inst.string[3*stmnt.io_len] = 0;
				args[0] = fmt_inst.out;
			}
			for (uint32_t i = 0; i < stmnt.io_len; i++) {
				args[i + 1] = lower_expr(stmnt.io_args[i], b, next_var, arena);
			}
			struct ir_inst inst = {
				.kind = IR_OUTPUT,
				.args = args,
				.arg_count = stmnt.io_len + 1
			};
			insert(b, &inst);
			return;
		}
	default:
		log_unreachable();
	}
}

static void lower_block(struct ast_stmnt *stmnts, uint32_t *next_var, uint32_t len, struct mem_arena *arena, struct builder *bld)
{
	bld->fun->bb[bld->bb] = (struct ir_bb) {
		.inst_count = 0,
		.inst_back = ((len - 1) / 16 + 1) * 16,
		.term = (struct ir_term) { .kind = IR_RET }
	};
	bld->fun->bb[bld->bb].insts = xmalloc(sizeof(struct ir_inst) * bld->fun->bb[bld->bb].inst_back);

	for (uint32_t i = 0; i < len; i++) {
		switch (stmnts[i].kind) {
		case AST_IF:
			lower_if(stmnts+i, next_var, len-i, arena, bld);
			return;
		case AST_WHILE:
			lower_while(stmnts+i, next_var, len-i, arena, bld);
			return;
		default:
			lower_stmnt(stmnts[i], bld, next_var, arena);
		}
	}
}

static void lower_program(struct ast_top prog, struct mem_arena *arena)
{
	uint32_t next_var = 1;
	struct ir_fun fun = {
		.sym = mangle_program(fe_symbols[prog.prog_name]),
		.loc = {
			.file = prog.loc->file,
			.line = prog.loc->line
		},
		.type = { .kind = IR_VOID },
		.bb = xmalloc(4 * sizeof(struct ir_bb)),
		.bb_back = 4,
		.bb_count = 1
	};
	struct builder bld = { .fun = &fun, .bb = 0 };
	lower_block(prog.prog_stmnts, &next_var, prog.prog_len, arena, &bld);
	struct ir_entry entry = {
		.name = fe_symbols[prog.prog_name],
		.fun = ir_fun_count
	};
	array_insert((void **) &ir_funs, &ir_fun_count, &ir_fun_back, &fun, sizeof(struct ir_fun));
	array_insert((void **) &ir_entries, &ir_entry_count, &ir_entry_back, &entry, sizeof(struct ir_entry));
}

static void lower_procedure(struct ast_top proc, struct mem_arena *arena)
{
	struct ir_fun fun = {
		.sym = mangle_proc(proc),
		.loc = {
			.file = proc.loc->file,
			.line = proc.loc->line
		},
		.args = xmalloc(proc.proc_arg_len * sizeof(struct ir_type)),
		.arg_count = proc.proc_arg_len,
		.bb = xmalloc(4 * sizeof(struct ir_bb)),
		.bb_back = 4,
		.bb_count = 2
	};
	for (uint32_t i = 0; i < proc.proc_arg_len; i++) {
		fun.args[i] = lower_type(proc.proc_arg_types[i], arena);
	}
	if (proc.kind == AST_FUNCTION) {
		fun.type = lower_type(proc.fun_type, arena);
	}
	uint32_t next_var = proc.proc_arg_len + 1;

	if (proc.kind == AST_FUNCTION) {
		if (proc.proc_id >= fun_to_fun_back) {
			fun_to_fun_back = fun_to_fun_back ? fun_to_fun_back : 16;
			fun_to_fun_back *= 2;
			fun_to_fun = xrealloc(fun_to_fun, fun_to_fun_back*sizeof(uint32_t));
		}
		fun_to_fun[proc.proc_id] = ir_fun_count;
	} else if (proc.kind == AST_SUBROUTINE) {
		if (proc.proc_id >= sub_to_fun_back) {
			sub_to_fun_back = sub_to_fun_back ? sub_to_fun_back : 16;
			sub_to_fun_back *= 2;
			sub_to_fun = xrealloc(sub_to_fun, sub_to_fun_back*sizeof(uint32_t));
		}
		sub_to_fun[proc.proc_id] = ir_fun_count;
	} else if (proc.kind == AST_OP_FUNCTION) {
		if (proc.proc_id >= fun_op_to_fun_back) {
			fun_op_to_fun_back = fun_op_to_fun_back ? fun_op_to_fun_back : 16;
			fun_op_to_fun_back *= 2;
			fun_op_to_fun = xrealloc(fun_op_to_fun, fun_op_to_fun_back*sizeof(uint32_t));
		}
		fun_op_to_fun[proc.proc_id] = ir_fun_count;
	} else {
		if (proc.proc_id >= fun_alphaop_to_fun_back) {
			fun_alphaop_to_fun_back = fun_alpaop_to_fun_back ? fun_alphaop_to_fun_back : 16;
			fun_alphaop_to_fun_back *= 2;
			fun_alphaop_to_fun = xrealloc(fun_alphaop_to_fun, fun_alphaop_to_fun_back*sizeof(uint32_t));
		}
		fun_alphaop_to_fun[proc.proc_id] = ir_fun_count;
	}

	// argument saving basic block
	struct ir_bb bb0 = (struct ir_bb) {
		.inst_count = 0,
		.inst_back = ((proc.proc_len - 1) / 16 + 1) * 16
	};
	bb0.insts = xmalloc(sizeof(struct ir_inst) * bb0.inst_back);

	for (uint32_t i = 0; i < proc.proc_arg_len; i++) {
		uint32_t p = next_var;
		struct ir_inst inst = {
			.kind = IR_DECLARE,
			.out = p,
			.type = { .kind = IR_LINPTR, .arg = xmalloc(sizeof(struct ir_type)) }
		};
		*inst.type.arg = lower_type(proc.proc_arg_types[i], arena);
		hash_table_set(&local_type_table, &proc.proc_arg_names[i], inst.type.arg);
		bb_insert(&bb0, &inst);
		next_var++;

		inst = (struct ir_inst) {
			.kind = IR_STORE,
			.argn = { p, i+1 }
		};
		bb_insert(&bb0, &inst);

		hash_table_set(&local_table, &proc.proc_arg_names[i], &p);
	}

	bb0.term = (struct ir_term) {
		.kind = IR_GOTO,
		.target = 1
	};
	fun.bb[0] = bb0;

	struct builder bld = { .fun = &fun, .bb = 1 };
	lower_block(proc.proc_stmnts, &next_var, proc.proc_len, arena, &bld);
	if (proc.kind == AST_FUNCTION) {
		struct ir_inst inst = (struct ir_inst) {
			.kind = IR_LOAD,
			.arg = lookup(proc.proc_name),
			.out = next_var,
			.type = lookup_type(proc.proc_name)
		};
		insert(&bld, &inst);
		bld.fun->bb[bld.bb].term = (struct ir_term) {
			.kind = IR_RET,
			.ret_value = next_var
		};
		next_var++; // probably dead, but clang's got LLVM on its side
	} else {
		bld.fun->bb[bld.bb].term = (struct ir_term) {
			.kind = IR_RET
		};
	}

	array_insert((void **) &ir_funs, &ir_fun_count, &ir_fun_back, &fun, sizeof(struct ir_fun));
}

void lower(struct ast_top node, struct mem_arena *arena)
{
	switch (node.kind) {
	case AST_EOF:
		break;
	case AST_PROGRAM:
		lower_program(node, arena);
		break;
	case AST_FUNCTION:
	case AST_SUBROUTINE:
	case AST_OP_FUNCTION:
	case AST_ALPHAOP_FUNCTION:
		lower_procedure(node, arena);
		break;
	case AST_MACRO:
		log_unreachable();
	}

	hash_table_clean(&local_table);
	hash_table_clean(&local_type_table);
}
