#include "frontend/lexer.h"
#include "frontend/frontend.h"
#include "util/log.h"
#include "util/xmalloc.h"
#include "util/array.h"
#include "frontend/error.h"
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

const char *lex_token_name[] = {
	"EOF",
	"NEWLINE",
	"COMMA",
	"RoundLeftBracket",
	"RoundRightBracket",
	"EQSIGN",
	"DBLCOLON",
	"DBLSTAR",
	"STAR",
	"SLASH",
	"BACKSLASH",
	"PLUS",
	"MINUS",
	"COLON",
	"DBLEQ",
	"NEQ",
	"LT",
	"LE",
	"GE",
	"GT",
	"ALPHAOP",
	"SYMBOL",
	"STRING",
	"INTEGER",
	"END",
	"PROGRAM",
	"OUTPUT",
	"TRUE",
	"FALSE",
	"FUNCTION",
	"SUBROUTINE",
	"RETURN",
	"NOT",
	"AND",
	"OR",
	"WHILE",
	"ELSE",
	"IF",
};

// TODO: actually intern filenames instead of xmallocing each time

// for error messages
#define GEN_LOC(state) ((struct ast_location) { .file = state->file, .line = state->line, .col = state->col })
// check for no eof within n characters
#define NO_EOF(state, where, n) error_assert((state)->i+(n) < fe_text_length, GEN_LOC(state), "Unnexpected EOF in %s", where)

// case insensitive string equality up to given length
// a must be null terminated, b has given length
static bool ci_str_eq(const char *a, const char *b, size_t len)
{
	for (size_t i = 0; i < len; i++) {
		if (a[i] == 0) { // reached end of a but still have b
			return false;
		} else if (tolower(a[i]) != tolower(b[i])) {
			return false;
		}
	}
	return a[len] == 0;
}

struct keyword {
	const char *name; // lowercase
	enum lex_token_kind kind;
};
#define KEYWORD_COUNT 14
static const struct keyword keyword[KEYWORD_COUNT] = {
	{ "program", LEX_PROGRAM },
	{ "end", LEX_END },
	{ "output", LEX_OUTPUT },
	{ "true", LEX_TRUE },
	{ "false", LEX_FALSE },
	{ "function", LEX_FUNCTION },
	{ "subroutine", LEX_SUBROUTINE },
	{ "return", LEX_RETURN },
	{ "not", LEX_NOT },
	{ "and", LEX_AND },
	{ "or", LEX_OR },
	{ "else", LEX_ELSE },
	{ "if", LEX_IF },
	{ "while", LEX_WHILE },
};

static struct lex_token symbol(struct lex_state *state)
{
	struct lex_state start = *state;

	while (state->i < fe_text_length && (isalnum(fe_text[state->i]) || fe_text[state->i] == '_')) {
		state->i++;
		state->col++;
	}

	size_t len = state->i - start.i;

	for (int i = 0; i < KEYWORD_COUNT; i++) {
		if (ci_str_eq(keyword[i].name, fe_text+start.i, len)) {
			return (struct lex_token) {
				.kind = keyword[i].kind,
				.file = state->file,
				.line = start.line,
				.col = start.col
			};
		}
	}

	return (struct lex_token) {
		.kind = LEX_SYMBOL,
		.file = state->file,
		.line = start.line,
		.col = start.col,
		.symbol = fe_add_symbol(fe_text+start.i, len)
	};
}

static struct lex_token alphaop(struct lex_state *state)
{
	struct lex_state start = *state;
	state->i++;
	state->col++;

	size_t i0 = state->i;
	while (state->i < fe_text_length && (isalnum(fe_text[state->i]) || fe_text[state->i] == '_')) {
		state->i++;
		state->col++;
	}
	size_t len = state->i - i0;
	error_assert(state->i < fe_text_length && fe_text[state->i] == '.', GEN_LOC(state),
			"Alphanumeric operator must be between %s", "periods");
	state->i++;
	state->col++;

	return (struct lex_token) {
		.kind = LEX_ALPHAOP,
		.file = state->file,
		.line = start.line,
		.col = start.col,
		.symbol = fe_add_symbol(fe_text+i0, len)
	};
}

static unsigned char dehex(char c)
{
	if (c >= '0' && c <= '9') {
		return c - '0';
	} else if (c >= 'a' && c <= 'f') {
		return c - 'a' + 10;
	} else if (c >= 'A' && c <= 'F') {
		return c - 'A' + 10;
	} else {
		log_unreachable();
	}
}

// TODO: handle escapes (at all), consider interning
static struct lex_token string(struct lex_state *state)
{
	struct lex_state start = *state;

	while (state->i < fe_text_length && fe_text[state->i] != '"') {
		if (fe_text[state->i] == '\\') {
			NO_EOF(state, "string", 1);
			state->i++;
			state->col++;
		}
		if (fe_text[state->i] == '\n' || fe_text[state->i] == '\r' || fe_text[state->i] == '\v') {
			state->line++;
			state->col = 1;
		} else {
			state->col++;
		}
		state->i++;
	}

	NO_EOF(state, "string", 0);
	state->i++; // drop final double quote

	size_t len = state->i - start.i - 1;
	char *str = xmalloc(len + 1);
	size_t j = 0; // insert location, i is read location
	for (size_t i = start.i; i < start.i+len; i++) {
		if (fe_text[i] == '\\') {
			i++;
			// TODO: UTF-8 escapes
			switch (fe_text[i]) {
			case 'a':
				str[j] = '\a';
				break;
			case 'b':
				str[j] = '\b';
				break;
			case 'f':
				str[j] = '\f';
				break;
			case 'n':
				str[j] = '\n';
				break;
			case 'r':
				str[j] = '\r';
				break;
			case 't':
				str[j] = '\t';
				break;
			case 'v':
				str[j] = '\v';
				break;
			case '\\':
			case '"':
			case '\'':
				str[j] = fe_text[i];
				break;
			case '\n':
			case '\r':
			case '\v':
				j--;
				break;
			case 'x':
				// TODO: proper location reporting
				i++;
				str[j] = dehex(fe_text[i]) * 0x10 + dehex(fe_text[i+1]);
				break;
			default:
				error(GEN_LOC(state), "Unrecognised escape character %c", fe_text[i]);
			}
			j++;
		} else {
			str[j] = fe_text[i];
			j++;
		}
	}
	str[j] = 0;

	return (struct lex_token) {
		.kind = LEX_STRING,
		.file = state->file,
		.line = start.line,
		.col = start.col,
		.string = str
	};
}

static struct lex_token integer(struct lex_state *state, int sign)
{
	struct lex_state start = *state;
	int64_t n = 0;

	while (state->i < fe_text_length && isdigit(fe_text[state->i])) {
		n = n*10 + fe_text[state->i] - '0';
		state->i++;
		state->col++;
	}

	return (struct lex_token) {
		.kind = LEX_INTEGER,
		.file = state->file,
		.line = start.line,
		.col = start.col,
		.integer = n * sign
	};
}

struct lex_token lex(struct lex_state *state)
{
	if (state->next_count) {
		struct lex_token r = state->next[0];
		state->next_count--;
		return r;
	}

	// the goto-based FSM here
	// eats whitespace and preprocessor directives

	uint32_t line_tmp = 0; // line number in building
	struct lex_state lfn_start; // lexer state at start of line filename
	struct lex_token result; // used for some returns (but not all)

// single character operator case handling
#define CHAR_OP(c, k) \
	case c: \
		result = (struct lex_token) { \
			.kind = k, \
			.file = state->file, \
			.line = state->line, \
			.col = state->col \
		}; \
		state->i++; \
		state->col++; \
		return result;

entry:
	if (state->i >= fe_text_length) {
		return (struct lex_token) { .kind = LEX_EOF };
	}
	switch (fe_text[state->i]) {
	case ' ': case '\t':
		state->i++;
		state->col++;
		goto entry;
	case '\n': case '\r': case '\v': // TODO: don't double lines on DOS formatting
		result = (struct lex_token) {
			.kind = LEX_NEWLINE,
			.file = state->file,
			.line = state->line,
			.col = state->col
		};
		state->i++;
		state->col = 1;
		state->line++;
		return result;
	CHAR_OP(',', LEX_COMMA)
	case '=':
		result = (struct lex_token) {
			.file = state->file,
			.line = state->line,
			.col = state->col
		};
		// a bit overzealous, but it's going to be a syntax error by parsing
		NO_EOF(state, "operator starting with equal sign", 2);
		state->i++;
		state->col++;
		switch (fe_text[state->i]) {
		case '=':
			result.kind = LEX_DBLEQ;
			state->i++;
			state->col++;
			return result;
		case '/':
		case '\\':
			if (fe_text[state->i + 1] == '=') {
				result.kind = LEX_NEQ;
				state->i += 2;
				state->col += 2;
				return result;
			} else {
				result.kind = LEX_EQSIGN;
				return result;
			}
		default:
			result.kind = LEX_EQSIGN;
			return result;
		}
	case '*':
		if (state->i + 1 < fe_text_length && fe_text[state->i + 1] == '*') {
			result = (struct lex_token) {
				.kind = LEX_DBLSTAR,
				.file = state->file,
				.line = state->line,
				.col = state->col
			};
			state->i += 2;
			state->col += 2;
			return result;
		} else {
			result = (struct lex_token) {
				.kind = LEX_STAR,
				.file = state->file,
				.line = state->line,
				.col = state->col
			};
			state->i++;
			state->col++;
			return result;
		}
	CHAR_OP('/', LEX_SLASH)
	CHAR_OP('\\', LEX_BACKSLASH)
	CHAR_OP('+', LEX_PLUS)
	CHAR_OP('(', LEX_RLB)
	CHAR_OP(')', LEX_RRB)
	case '-':
		if (state->i + 1 < fe_text_length && isdigit(fe_text[state->i + 1])) {
			state->i++;
			state->col++;
			return integer(state, -1);
		} else {
			result = (struct lex_token) {
				.kind = LEX_MINUS,
				.file = state->file,
				.line = state->line,
				.col = state->col
			};
			state->i++;
			state->col++;
			return result;
		}
	case ':':
		if (state->i + 1 >= fe_text_length || fe_text[state->i + 1] != ':') {
			result = (struct lex_token) {
				.kind = LEX_COLON,
				.file = state->file,
				.line = state->line,
				.col = state->col
			};
			state->i++;
			state->col++;
			return result;
		} else {
			result = (struct lex_token) {
				.kind = LEX_DBLCOLON,
				.file = state->file,
				.line = state->line,
				.col = state->col
			};
			state->i += 2;
			state->col += 2;
			return result;
		}
	case '<':
		if (state->i + 1 < fe_text_length && fe_text[state->i + 1] == '=') {
			result = (struct lex_token) {
				.kind = LEX_LE,
				.file = state->file,
				.line = state->line,
				.col = state->col
			};
			state->i += 2;
			state->col += 2;
			return result;
		} else {
			result = (struct lex_token) {
				.kind = LEX_LT,
				.file = state->file,
				.line = state->line,
				.col = state->col
			};
			state->i++;
			state->col++;
			return result;
		}
	case '>':
		if (state->i + 1 < fe_text_length && fe_text[state->i + 1] == '=') {
			result = (struct lex_token) {
				.kind = LEX_GE,
				.file = state->file,
				.line = state->line,
				.col = state->col
			};
			state->i += 2;
			state->col += 2;
			return result;
		} else {
			result = (struct lex_token) {
				.kind = LEX_GT,
				.file = state->file,
				.line = state->line,
				.col = state->col
			};
			state->i++;
			state->col++;
			return result;
		}
	case '.':
		return alphaop(state);
	case '#':
		error_assert(state->col == 1, GEN_LOC(state),
				"Preprocessor directive not at start of line, but column %u",
				(unsigned) state->col);
		state->col++;
		state->i++;
		goto directive;
	case '"':
		state->i++;
		return string(state);
	case '!':
		state->i++;
		state->col++;
		goto comment;
	default:
		if (isdigit(fe_text[state->i])) return integer(state, 1);
		error_assert(isalpha(fe_text[state->i]), GEN_LOC(state),
				"Lexer doesn't know what to do with %c", fe_text[state->i]);
		[[fallthrough]];
	case '_':
		return symbol(state);
	}

directive:
	NO_EOF(state, "preprocessor directive", 0);
	switch (fe_text[state->i]) { // TODO: incorporate C preprocessor stuff
	case ' ': case '\t':
		state->i++;
		state->col++;
		goto directive;
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
		line_tmp = 0;
		goto line_directive_number;
	default:
		error(GEN_LOC(state), "Invalid character in proprocessor directive: %c", fe_text[state->i]);
	}

line_directive_number:
	// column updating unnecessary
	NO_EOF(state, "line directive", 0);
	if (isdigit(fe_text[state->i])) {
		line_tmp = line_tmp*10 + fe_text[state->i] - '0';
		state->i++;
		goto line_directive_number;
	} else {
		error_assert(fe_text[state->i] == ' ' || fe_text[state->i] == '\t', GEN_LOC(state),
				"Malformed line directive: got %c", fe_text[state->i]);
		state->i++;
		state->col++;
		NO_EOF(state, "line directive", 0);
		error_assert(fe_text[state->i] == '"', GEN_LOC(state),
				"Malformed line directive: expected \" got %c", fe_text[state->i]);
		state->i++;
		lfn_start = *state;
		goto line_directive_filename;
	}

line_directive_filename:
	// TODO: backslashes not copxied verbatim into filename
	NO_EOF(state, "line directive", 0);
	if (fe_text[state->i] == '"') {
		size_t len = state->i - lfn_start.i + 1;
		char *file = xmalloc(len + 1);
		memcpy(file, fe_text+lfn_start.i, len);
		file[len] = 0;
		state->file = file;
		state->i++;
		state->line = line_tmp - 1; // counteract end_of_line advancing state->line
		goto end_of_line;
	} else if (fe_text[state->i] == '\\') {
		state->i += 2;
		goto line_directive_filename;
	} else {
		state->i++;
		goto line_directive_filename;
	}

end_of_line:
	if (state->i >= fe_text_length) {
		return (struct lex_token) { .kind = LEX_EOF };
	} else if (fe_text[state->i] == '\n' || fe_text[state->i] == '\r' || fe_text[state->i] == '\v') {
		state->i++;
		state->line++;
		state->col = 1;
		goto entry;
	} else {
		state->i++;
		goto end_of_line;
	}

comment:
	if (state->i >= fe_text_length) {
		return (struct lex_token) { .kind = LEX_EOF };
	} else if (fe_text[state->i] == '\n' || fe_text[state->i] == '\r' || fe_text[state->i] == '\v') {
		result = (struct lex_token) {
			.kind = LEX_NEWLINE,
			.file = state->file,
			.line = state->line,
			.col = state->col
		};
		state->i++;
		state->col = 1;
		state->line++;
		return result;
	} else {
		state->i++;
		goto comment;
	}
}

void unlex(struct lex_state *state, const struct lex_token *tok)
{
	array_insert32((void **) &state->next, &state->next_count, &state->next_back, tok, sizeof(struct lex_token));
}
