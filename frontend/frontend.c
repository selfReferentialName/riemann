#include "frontend/frontend.h"
#include "frontend/ast.h"
#include "frontend/lower.h"
#include "util/log.h"
#include "util/xmalloc.h"
#include "util/array.h"
#include <stdio.h>
#include <string.h>

char **fe_symbols;
size_t fe_symbol_count;
size_t fe_symbol_back;
struct hash_table_vlk fe_symbol_table = {
	.valsize = sizeof(uint32_t)
};

char *fe_text;
size_t fe_text_length;

uint32_t fe_sym_text;
uint32_t fe_sym_int;
uint32_t fe_sym_integer;
uint32_t fe_sym_logical;
uint32_t fe_sym_logic;
uint32_t fe_sym_result;
uint32_t fe_sym_mod;

uint32_t fe_add_symbol(const char *name, size_t len)
{
	uint32_t r;
	if (hash_table_vlk_get(&fe_symbol_table, name, len, &r)) {
		return r;
	} else {
		r = fe_symbol_count;
		char *sym = xmalloc(len + 1);
		memcpy(sym, name, len);
		sym[len] = 0;
		array_insert((void **) &fe_symbols, &fe_symbol_count, &fe_symbol_back, &sym, sizeof(char *));
		hash_table_vlk_set(&fe_symbol_table, name, len, &r);
		return r;
	}
}

bool frontend_top(struct ast_top ast, struct mem_arena *arena)
{
	switch (ast.kind) {
	case AST_EOF:
		mem_arena_destroy(arena);
		// this line was in the original which was inline in frontend
		// if (state.next) free(state.next);
		// if not doing this causes any memory leaks, too bad
		return false;
	case AST_MACRO:
		macro_register(ast, arena);
		break;
	default:
		type_ast(&ast, arena);
		lower(ast, arena);
	}
	return true;
}

void frontend(const char *filename)
{
	FILE *file = fopen(filename, "r");
	log_assert(file);
	log_assert(!fseek(file, 0, SEEK_END));
	fe_text_length = ftell(file);
	log_assert(fe_text_length != -1);
	log_assert(!fseek(file, 0, SEEK_SET));
	fe_text = xmalloc(fe_text_length);
	log_assert(fread(fe_text, 1, fe_text_length, file));
	log_assert(!fclose(file));

	fe_add_symbol("", 0); // a dummy symbol so no symbol takes the value 0
	fe_sym_text = fe_add_symbol("TEXT", 4);
	fe_sym_int = fe_add_symbol("INT", 3);
	fe_sym_integer = fe_add_symbol("INTEGER", 7);
	fe_sym_logical = fe_add_symbol("LOGICAL", 7);
	fe_sym_logic = fe_add_symbol("LOGIC", 5);
	fe_sym_result = fe_add_symbol("RESULT", 6);
	fe_sym_mod = fe_add_symbol("mod", 3);

	struct lex_state state = {
		.file = filename,
		.line = 1,
		.col = 1
	};
	struct mem_arena *arena = mem_arena_new(1024);

	bool running = true;
	while (running) {
		struct ast_top ast = parse(&state, arena);
		running = frontend_top(ast, arena);
		mem_arena_reset(arena);
	}
}
