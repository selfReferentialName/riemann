#include "frontend/error.h"
#include <stdio.h>
#include <stdlib.h>

static const char *kind_name[] = {
	[ERROR] = "ERR",
	[WARNING] = "WRN",
};

void err(enum error_kind kind, struct ast_location loc, const char *fmt, const char *file, size_t line, ...)
{
	va_list ap;
	va_start(ap, line);
	fprintf(stderr, "[%s]: ", kind_name[kind]);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fprintf(stderr, "\n    at: %s line %llu column %llu\n", loc.file,
			(unsigned long long) loc.line, (unsigned long long) loc.col);
	for (struct ast_location *cur = loc.next; cur; cur = cur->next) {
		fprintf(stderr, "    related to %s line %llu column %llu\n",
				cur->file, (unsigned long long) cur->line, (unsigned long long) cur->col);
	}
	fprintf(stderr, "    reported from %s:%llu\n", file, (unsigned long long) line);
	switch (kind) {
	case ERROR:
		exit(1);
	default:
		return;
	}
}
