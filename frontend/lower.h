#ifndef FRONTEND_LOWER_H
#define FRONTEND_LOWER_H

#include "frontend/ast.h"

/// Lower a top level node
void lower(struct ast_top node, struct mem_arena *arena);

#endif
