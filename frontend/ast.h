#ifndef FRONTEND_AST_H
#define FRONTEND_AST_H

#include "frontend/lexer.h"
#include "util/arena.h"
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

/// The location information for error reporting
struct ast_location {
	size_t length; //< the length
	const char *file; //< what __FILE__ would expand to
	struct ast_location *next; //< next location to report (macro usage location)
	uint32_t line; //< what __LINE__ would expand to
	uint32_t col; //< the starting column in the line
};

/// The various kinds of types
enum ast_type_kind {
	TAST_TEXT, //< a string
	TAST_INTEGER, //< an integer
	TAST_LOGIC, //< a logical
};
/// Type information
struct ast_type {
	enum ast_type_kind kind; //< the kind of type
};

/// The various kinds of expressions
enum ast_expr_kind {
	AST_VARIABLE, //< using the value of a variable
	AST_STRING, //< a string litteral
	AST_INTEGER, //< an integer litteral
	AST_LOGIC, //< a true or false
	AST_FCALL, //< calling a function (function is first argument)
	AST_FUNUSE, //< a function by ID
	AST_OP, //< a non-alphanumeric (or short circuting) binary operation
	AST_ALPHAOP, //< a alphanumeric binary operation
	AST_NOT, //< logical negation
	AST_NEG, //< arithmatic negation
};
/// The various binary operators
enum ast_operator {
	AST_POW, //< **
	AST_TIMES, //< *
	AST_RDIV, //< /
	AST_LDIV, //< "\"
	AST_ADD, //< +
	AST_SUB, //< -
	AST_EQ, //< ==
	AST_NEQ, //< =\=
	AST_LT, //< <
	AST_LE, //< <=
	AST_GE, //< >=
	AST_GT, //< >
	AST_AND, //< AND
	AST_OR, //< OR
};
/// The data and associated expression node
struct ast_expr {
	struct ast_location *loc; //< the location of the node
	struct ast_type *type; //< the type (can be NULL unless in tast)
	union {
		uint32_t var; //< the variable to use
		uint32_t id; //< TAST: ID of a function
		char *string; //< the data of a string litteral
		int64_t integer; //< the data of an integer litteral
		bool logic; //< the data of a logical litteral
		struct {
			size_t argc; //< generic argument count
			struct ast_expr *args; //< generic argument list
		};
		struct ast_expr *arg; //< generic unary argument
		struct {
			struct ast_expr *lhs; //< left of binary operator
			struct ast_expr *rhs; //< right of binary operator
			union {
				enum ast_operator op; //< built in binary operator
				uint32_t alphaop; //< alphanumeric binary operator
			};
			uint32_t op_id; //< ID of operator
		};
	};
	enum ast_expr_kind kind;
};

/// The various kinds of statements
enum ast_stmnt_kind {
	AST_ASSIGNMENT, //< assignment (v = e)
	AST_DECLARATION, //< a declaration (v :: t or v = e :: t)
	AST_OUTPUT, //< an OUTPUT statement
	AST_SCALL, //< calling a subroutine
	AST_RETURN, //< return from a function
	AST_IF, //< an IF statement
	AST_WHILE, //< a WHILE statement
};
/// The data and associated statement node
struct ast_stmnt {
	struct ast_location *loc; //< the location of the node
	union {
		struct {
			struct ast_expr *format; //< INPUT and OUTPUT format, can be NULL for *
			struct ast_expr *io_args; //< What to INPUT/OUTPUT
			size_t io_len; //< Number of io_args
		};
		struct {
			struct ast_type decl_type; //< Type of declared variable
			struct ast_expr *decl_init; //< Initial value of declaration, can be NULL
			uint32_t decl_name; //< Name of variable to declare
		};
		struct {
			struct ast_expr *set_value; //< The value to assign
			uint32_t set_name; //< The variable to assign to
		};
		struct {
			struct ast_expr *call_args; //< Subroutine arguments
			size_t call_len; //< Number of call_args
			uint32_t sub; //< Subroutine to call
			uint32_t sub_id; //< TAST: ID of subroutine to call
		};
		struct {
			union {
				struct {
					struct ast_expr *if_cond; //< Condition for IF statement
					struct ast_stmnt *if_else; //< Statements for ELSE, maybe NULL
					uint32_t if_else_len; //< Length of if_else
				};
				struct ast_expr *while_cond; //< Condition for WHILE statement
			};
			struct ast_stmnt *stmnts; //< Statements of a statement with a block
			uint32_t block_len; //< Length of stmnts
		};
		struct ast_expr *e; //< generic expression data
	};
	enum ast_stmnt_kind kind;
};

/// The various kinds of top level nodes
enum ast_top_kind {
	AST_EOF, //< when there's no more stuff to parse
	AST_PROGRAM, //< a PROGRAM
	AST_FUNCTION, //< a FUNCTION
	AST_OP_FUNCTION, //< a FUNCTION for operator overloading of symbolic
	AST_ALPHAOP_FUNCTION, //< a FUNCTION for alphanumeric operator
	AST_SUBROUTINE, //< a SUBROUTINE
	AST_MACRO, //< a basic MACRO
};
/// The data and associated top level node
struct ast_top {
	struct ast_location *loc; //< the location of the node
	union {
		struct {
			struct ast_stmnt *prog_stmnts; //< the statements of a PROGRAM
			uint32_t prog_name; //< the name of a PROGRAM
			uint32_t prog_len; //< the number of statements in a PROGRAM
		};
		struct {
			struct ast_type fun_type; //< in TAST: the return type of a FUNCTION
			struct ast_stmnt *proc_stmnts; //< the statements of a FUNCTION/SUBROUTINE
			uint32_t *proc_arg_names; //< the variables of the arguments of a FUNCTION/SUBROUTINE
			struct ast_type *proc_arg_types; // in TAST: the types of the arguments
			union {
				uint32_t proc_name; //< the name of a FUNCTION/SUBROUTINE/alphanumeric operator
				enum ast_operator proc_op; //< the name of a FUNCTION operator
			};
			uint32_t proc_len; //< the number of statements in a FUNCTION/SUBROUTINE
			uint32_t proc_arg_len; //< the number of arguments to a FUNCTION/SUBROUTINE
			uint32_t proc_id; //< TAST: unique procedure ID (0 in operators is built in)
		};
		struct {
			struct lex_token *macro_args; //< MACRO argument names or bound tokens
			struct lex_token *macro_text; //< MACRO text to expand into
			_BitInt(33) macro_argc; //< acceptable number of MACRO arguments, ones complement negative for varadic
			uint32_t macro_name; //< the bound name of a MACRO
			uint32_t macro_len; //< length of macro_text
		};
	};
	enum ast_top_kind kind; //< what kind of node this is
};

/** @brief Parse the input file, calling the lexer
 *
 * Returns the next top-level node. Node stuff is allocated in
 * the given arena.
 *
 * @param state The state of the lexer.
 * @param arena The arena to put the output in. Also used for some lexing stuff.
 *
 * @return The next top-level node, or an AST_EOF node
 */
struct ast_top parse(struct lex_state *state, struct mem_arena *arena);

/// Convert ast into typed ast in place
void type_ast(struct ast_top *top, struct mem_arena *arena);

#endif
