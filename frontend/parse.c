#include "frontend/ast.h"
#include "frontend/lexer.h"
#include "frontend/frontend.h"
#include "frontend/error.h"
#include "util/arena.h"
#include "util/log.h"

#define WANT_NEWLINE(state, loc, where) error_assert(lex(state).kind == LEX_NEWLINE, loc, "%s not terminated with newline", where)

// get the location of tok given the ending state
static struct ast_location get_location(struct lex_token tok)
{
	return (struct ast_location) {
		.file = tok.file,
		.line = tok.line,
		.col = tok.col
	};
}

// lex newlines, return the first non-newline
static struct lex_token skip_newlines(struct lex_state *state)
{
	struct lex_token tok;
	do {
		tok = lex(state);
	} while (tok.kind == LEX_NEWLINE);
	return tok;
}

static struct ast_expr parse_expr(struct lex_state *state, struct mem_arena *arena, uint8_t bp);

static struct ast_expr parse_fcall(struct ast_expr fun, struct lex_state *state, struct mem_arena *arena, struct ast_location *loc)
{
	struct ast_expr *args = mem_arena_alloc(arena, 2, struct ast_expr);
	size_t back = 2;
	args[0] = fun;
	size_t len = 1;

	struct lex_token tok = lex(state);
	while (tok.kind != LEX_RRB && tok.kind != LEX_EOF) {
		unlex(state, &tok);
		if (len >= back) {
			back *= 4;
			struct ast_expr *new_args = mem_arena_alloc(arena, back, struct ast_expr);
			memcpy(new_args, args, len * sizeof(struct ast_expr));
			args = new_args;
		}
		args[len] = parse_expr(state, arena, 0);
		len++;
		tok = lex(state);
		error_assert(tok.kind == LEX_RRB || tok.kind == LEX_COMMA, get_location(tok),
				"Argument list should be comma separated close paren terminated but got %s",
				lex_token_name[tok.kind]);
		if (tok.kind == LEX_COMMA) tok = lex(state);
	}
	error_assert(tok.kind == LEX_RRB, get_location(tok),
			"Argument list should be close parenthesis terminated but got %s",
			lex_token_name[tok.kind]);

	return (struct ast_expr) {
		.kind = AST_FCALL,
		.loc = loc,
		.args = args,
		.argc = len
	};
}

// {left, right} binding power
// 0 means not an operator in this usage
// prefix always has maximum precedence
// negation has precedence 220
// not has precedence 70
static uint8_t op_bp[2][LEX_IF] = {
	[0] = {
		[LEX_RLB] = 250,
		[LEX_DBLSTAR] = 231,
		[LEX_STAR] = 200,
		[LEX_SLASH] = 200,
		[LEX_BACKSLASH] = 200,
		[LEX_PLUS] = 150,
		[LEX_MINUS] = 150,
		[LEX_ALPHAOP] = 130,
//		[LEX_COLON] = 120,
		[LEX_DBLEQ] = 100,
		[LEX_NEQ] = 100,
		[LEX_LT] = 100,
		[LEX_LE] = 100,
		[LEX_GE] = 100,
		[LEX_GT] = 100,
		[LEX_AND] = 50,
		[LEX_OR] = 50,
	},
	[1] = {
		[LEX_RLB] = 0,
		[LEX_DBLSTAR] = 230,
		[LEX_STAR] = 201,
		[LEX_SLASH] = 201,
		[LEX_BACKSLASH] = 201,
		[LEX_PLUS] = 151,
		[LEX_MINUS] = 151,
		[LEX_ALPHAOP] = 131,
//		[LEX_COLON] = 121,
		[LEX_DBLEQ] = 101,
		[LEX_NEQ] = 101,
		[LEX_LT] = 101,
		[LEX_LE] = 101,
		[LEX_GE] = 101,
		[LEX_GT] = 101,
		[LEX_AND] = 51,
		[LEX_OR] = 51,
	}
};

static enum ast_operator operator_map[LEX_IF] = {
	[LEX_DBLSTAR] = AST_POW,
	[LEX_STAR] = AST_TIMES,
	[LEX_SLASH] = AST_RDIV,
	[LEX_BACKSLASH] = AST_LDIV,
	[LEX_PLUS] = AST_ADD,
	[LEX_MINUS] = AST_SUB,
	[LEX_DBLEQ] = AST_EQ,
	[LEX_NEQ] = AST_NEQ,
	[LEX_LT] = AST_LT,
	[LEX_LE] = AST_LE,
	[LEX_GE] = AST_GE,
	[LEX_GT] = AST_GT,
	[LEX_AND] = AST_AND,
	[LEX_OR] = AST_OR,
};

static struct ast_expr parse_expr(struct lex_state *state, struct mem_arena *arena, uint8_t bp)
{
	struct lex_token start = lex(state);
	struct ast_location *loc = mem_arena_alloc(arena, 1, struct ast_location);
	*loc = get_location(start);
	struct ast_expr lhs;

	switch (start.kind) {
	case LEX_STRING:
		lhs = (struct ast_expr) {
			.kind = AST_STRING,
			.loc = loc,
			.string = start.string
		};
		break;
	case LEX_INTEGER:
		lhs = (struct ast_expr) {
			.kind = AST_INTEGER,
			.loc = loc,
			.integer = start.integer
		};
		break;
	case LEX_SYMBOL:
		lhs = (struct ast_expr) {
			.kind = AST_VARIABLE,
			.loc = loc,
			.var = start.symbol
		};
		break;
	case LEX_TRUE:
	case LEX_FALSE:
		lhs = (struct ast_expr) {
			.kind = AST_LOGIC,
			.loc = loc,
			.logic = start.kind == LEX_TRUE
		};
		break;
	case LEX_MINUS:
		lhs = (struct ast_expr) {
			.kind = AST_NEG,
			.loc = loc,
			.arg = mem_arena_alloc(arena, 1, struct ast_expr)
		};
		*lhs.arg = parse_expr(state, arena, 220);
		break;
	case LEX_NOT:
		lhs = (struct ast_expr) {
			.kind = AST_NOT,
			.loc = loc,
			.arg = mem_arena_alloc(arena, 1, struct ast_expr)
		};
		*lhs.arg = parse_expr(state, arena, 70);
		break;
	case LEX_RLB:
		lhs = parse_expr(state, arena, 0);
		error_assert(lex(state).kind == LEX_RRB, *loc,
				"Parenthesis not closed %s", "properly");
		break;
	default:
		error(*loc, "Don't know how an expression can start with a %s", lex_token_name[start.kind]);
	}

	while (true) {
		struct lex_token op = lex(state);
		if (op_bp[0][op.kind] == 0 || op_bp[0][op.kind] < bp) {
			unlex(state, &op);
			return lhs;
		}

//		struct lex_token tok;
		if (op.kind == LEX_RLB) {
			lhs = parse_fcall(lhs, state, arena, loc);
		} else if (op.kind == LEX_ALPHAOP) {
			struct ast_expr *left = mem_arena_alloc(arena, 1, struct ast_expr);
			struct ast_expr *right = mem_arena_alloc(arena, 1, struct ast_expr);
			*left = lhs;
			*right = parse_expr(state, arena, op_bp[1][op.kind]);
			lhs = (struct ast_expr) {
				.loc = loc,
				.kind = AST_ALPHAOP,
				.lhs = left,
				.rhs = right,
				.alphaop = op.symbol
			};
		} else {
			struct ast_expr *left = mem_arena_alloc(arena, 1, struct ast_expr);
			struct ast_expr *right = mem_arena_alloc(arena, 1, struct ast_expr);
			*left = lhs;
			*right = parse_expr(state, arena, op_bp[1][op.kind]);
			lhs = (struct ast_expr) {
				.loc = loc,
				.kind = AST_OP,
				.lhs = left,
				.rhs = right,
				.op = operator_map[op.kind]
			};
		}
	}
}

static struct ast_type parse_type(struct lex_state *state, struct mem_arena *arena)
{
	struct lex_token start = lex(state);

	switch (start.kind) {
	case LEX_SYMBOL:
		if (start.symbol == fe_sym_text) {
			return (struct ast_type) { .kind = TAST_TEXT };
		} else if (start.symbol == fe_sym_int || start.symbol == fe_sym_integer) {
			return (struct ast_type) { .kind = TAST_INTEGER };
		} else if (start.symbol == fe_sym_logical || start.symbol == fe_sym_logic) {
			return (struct ast_type) { .kind = TAST_LOGIC };
		}
		error(get_location(start), "Unrecognised type name %s", fe_symbols[start.symbol]);
		/* fallthrough */
	default:
		error(get_location(start), "Don't know how type can start with %s", lex_token_name[start.kind]);
		exit(1);
	}
}

// OUTPUT must take at least one argument
static struct ast_stmnt parse_output(struct lex_state *state, struct mem_arena *arena, struct ast_location *loc)
{
	struct ast_expr *fmt;
	struct lex_token tok = lex(state);
	if (tok.kind == LEX_STAR) {
		fmt = NULL;
	} else {
		fmt = mem_arena_alloc(arena, 1, struct ast_expr);
		unlex(state, &tok);
		*fmt = parse_expr(state, arena, 0);
	}

	size_t arg_back = 16;
	size_t arg_count = 0;
	struct ast_expr *args = mem_arena_alloc(arena, 16, struct ast_expr);
	do {
		if (arg_back <= arg_count) {
			arg_back *= 2;
			struct ast_expr *new_args = mem_arena_alloc(arena, arg_back, struct ast_expr);
			memcpy(new_args, args, arg_count * sizeof(struct ast_expr));
			args = new_args;
		}
		args[arg_count] = parse_expr(state, arena, 0);
		arg_count++;
		tok = lex(state);
	} while (tok.kind == LEX_COMMA);
	error_assert(tok.kind == LEX_NEWLINE, get_location(tok), "Statement not terminated, got %s", lex_token_name[tok.kind]);

	return (struct ast_stmnt) {
		.kind = AST_OUTPUT,
		.loc = loc,
		.format = fmt,
		.io_args = args,
		.io_len = arg_count
	};
}

static struct ast_stmnt parse_scall(uint32_t symbol, struct lex_state *state, struct mem_arena *arena, struct ast_location *loc)
{
	struct ast_expr *args = mem_arena_alloc(arena, 2, struct ast_expr);
	size_t back = 2;
	size_t len = 0;

	struct lex_token tok = lex(state);
	while (tok.kind != LEX_RRB && tok.kind != LEX_EOF) {
		unlex(state, &tok);
		if (len >= back) {
			back *= 4;
			struct ast_expr *new_args = mem_arena_alloc(arena, back, struct ast_expr);
			memcpy(new_args, args, len * sizeof(struct ast_expr));
			args = new_args;
		}
		args[len] = parse_expr(state, arena, 0);
		len++;
		tok = lex(state);
		error_assert(tok.kind == LEX_RRB || tok.kind == LEX_COMMA, get_location(tok),
				"Arguments should be comma separated and end with close parenthesis but got %s",
				lex_token_name[tok.kind]);
		if (tok.kind == LEX_COMMA) tok = lex(state);
	}
	error_assert(tok.kind == LEX_RRB, get_location(tok),
			"Arguments should end with close parenthesis but got %s", lex_token_name[tok.kind]);
	error_assert(lex(state).kind == LEX_NEWLINE, get_location(tok),
			"Statement not terminated with %s", "newline");

	return (struct ast_stmnt) {
		.kind = AST_SCALL,
		.loc = loc,
		.sub = symbol,
		.call_args = args,
		.call_len = len
	};
}

static struct ast_stmnt parse_stmnt_symbol(uint32_t symbol, struct lex_state *state, struct mem_arena *arena, struct ast_location *loc)
{
	struct ast_stmnt result;
	struct ast_expr *expr;
	struct lex_token tok = lex(state);

	switch (tok.kind) {
	case LEX_DBLCOLON:
		result = (struct ast_stmnt) {
			.loc = loc,
			.kind = AST_DECLARATION,
			.decl_name = symbol,
			.decl_type = parse_type(state, arena)
		};
		error_assert(lex(state).kind == LEX_NEWLINE, *loc,
				"Statement not terminated with %s", "newline");
		return result;
	case LEX_EQSIGN:
		expr = mem_arena_alloc(arena, 1, struct ast_expr);
		*expr = parse_expr(state, arena, 0);
		tok = lex(state);
		switch (tok.kind) {
		case LEX_NEWLINE:
			return (struct ast_stmnt) {
				.loc = loc,
				.kind = AST_ASSIGNMENT,
				.set_name = symbol,
				.set_value = expr
			};
		case LEX_DBLCOLON:
			result = (struct ast_stmnt) {
				.loc = loc,
				.kind = AST_DECLARATION,
				.decl_name = symbol,
				.decl_type = parse_type(state, arena),
				.decl_init = expr
			};
			error_assert(lex(state).kind == LEX_NEWLINE, *loc,
					"Statement not terminated with %s", "newline");
			return result;
		default:
			error(*loc, "Malformed statement, looks like %s", "assignment");
		}
	case LEX_RLB:
		return parse_scall(symbol, state, arena, loc);
	default:
		error(*loc, "Malformed statement, unrecognised token %s after symbol", lex_token_name[tok.kind]);
		exit(1);
	}
}

static struct ast_stmnt parse_stmnt(struct lex_state *state, struct mem_arena *arena);

// parse the END line after the END itself
static void parse_end(struct lex_state *state, enum lex_token_kind kind, uint32_t name, bool has_name, char *what)
{
	struct lex_token tok = lex(state);
	error_assert(tok.kind == LEX_END, get_location(tok), "Expected end of %s, got %s", what, lex_token_name[tok.kind]);
	tok = lex(state);
	if (tok.kind == LEX_NEWLINE) {
		return;
	}
	error_assert(tok.kind == kind, get_location(tok), "End of %s marked wrong, got %s", what, lex_token_name[tok.kind]);
	tok = lex(state);
	if (tok.kind == LEX_NEWLINE) {
		return;
	} else if (has_name && tok.kind == LEX_SYMBOL) {
		error_assert(tok.symbol == name, get_location(tok),
				"End of %s marked with wrong name: %s != %s", fe_symbols[tok.symbol], fe_symbols[name]);
		WANT_NEWLINE(state, get_location(tok), "End line");
	} else {
		error(get_location(tok), "Trailing junk on end line of %s", what);
	}
}

struct block {
	struct ast_stmnt *stmnts;
	size_t count;
	size_t back;
};

static struct block parse_block(struct lex_state *state, struct mem_arena *arena, enum lex_token_kind *breaks, int break_count)
{
	struct lex_token sttok = skip_newlines(state);
	size_t stmnt_count = 0, stmnt_back = 16;
	struct ast_stmnt *stmnts = mem_arena_alloc(arena, stmnt_back, struct ast_stmnt);
	while (sttok.kind != LEX_END) {
		for (int i = 0; i < break_count; i++) {
			if (sttok.kind == breaks[i]) {
				goto done;
			}
		}
		unlex(state, &sttok);
		struct ast_stmnt stmnt = parse_stmnt(state, arena);
		if (stmnt_count >= stmnt_back) {
			stmnt_back *= 4;
			struct ast_stmnt *new_stmnts = mem_arena_alloc(arena, stmnt_back, struct ast_stmnt);
			memcpy(new_stmnts, stmnts, stmnt_count * sizeof(struct ast_stmnt));
		}
		stmnts[stmnt_count] = stmnt;
		stmnt_count++;
		sttok = skip_newlines(state);
	}
done:
	unlex(state, &sttok);
	return (struct block) {
		.stmnts = stmnts,
		.count = stmnt_count,
		.back = stmnt_back
	};
}

static struct ast_stmnt parse_if(struct lex_state *state, struct mem_arena *arena, struct ast_location *loc, bool want_end)
{
	struct ast_expr *cond = mem_arena_alloc(arena, 1, struct ast_expr);
	*cond = parse_expr(state, arena, 0);
	WANT_NEWLINE(state, *loc, "If statement start");

	enum lex_token_kind breaks[1] = { LEX_ELSE };
	struct block b = parse_block(state, arena, breaks, 1);

	struct ast_stmnt *if_else = NULL;
	uint32_t if_else_len = 0;
	struct lex_token tok = lex(state);
	if (tok.kind == LEX_ELSE) {
		tok = lex(state);
		if (tok.kind == LEX_IF) {
			if_else = mem_arena_alloc(arena, 1, struct ast_stmnt);
			struct ast_location *loc2 = mem_arena_alloc(arena, 1, struct ast_location);
			*loc2 = get_location(tok);
			*if_else = parse_if(state, arena, loc2, false);
			if_else_len = 1;
		} else {
			error_assert(tok.kind == LEX_NEWLINE, get_location(tok),
					"Expected IF or NEWLINE after ELSE, but got %s", lex_token_name[tok.kind]);
			unlex(state, &tok);
			struct block e = parse_block(state, arena, NULL, 0);
			if_else = e.stmnts;
			if_else_len = e.count;
		}
	} else {
		unlex(state, &tok);
	}

	if (want_end) parse_end(state, LEX_IF, 0, false, "if statement");

	return (struct ast_stmnt) {
		.kind = AST_IF,
		.loc = loc,
		.if_cond = cond,
		.if_else = if_else,
		.stmnts = b.stmnts,
		.if_else_len = if_else_len,
		.block_len = b.count
	};
}

static struct ast_stmnt parse_while(struct lex_state *state, struct mem_arena *arena, struct ast_location *loc)
{
	struct ast_expr *cond = mem_arena_alloc(arena, 1, struct ast_expr);
	*cond = parse_expr(state, arena, 0);
	WANT_NEWLINE(state, *loc, "While statement start");
	struct block b = parse_block(state, arena, NULL, 0);
	parse_end(state, LEX_WHILE, 0, false, "while statement");
	return (struct ast_stmnt) {
		.kind = AST_WHILE,
		.loc = loc,
		.while_cond = cond,
		.stmnts = b.stmnts,
		.block_len = b.count
	};
}

static struct ast_stmnt parse_return(struct lex_state *state, struct mem_arena *arena, struct ast_location *loc)
{
	struct lex_token tok = lex(state);
	if (tok.kind == LEX_NEWLINE) {
		return (struct ast_stmnt) {
			.kind = AST_RETURN,
			.loc = loc,
			.e = NULL
		};
	} else {
		unlex(state, &tok);
		struct ast_expr *val = mem_arena_alloc(arena, 1, struct ast_expr);
		*val = parse_expr(state, arena, 0);
		WANT_NEWLINE(state, *loc, "Return statement");
		return (struct ast_stmnt) {
			.kind = AST_RETURN,
			.loc = loc,
			.e = val
		};
	}
}

static struct ast_stmnt parse_stmnt(struct lex_state *state, struct mem_arena *arena)
{
	struct lex_token start = lex(state);
	struct ast_location *loc = mem_arena_alloc(arena, 1, struct ast_location);
	*loc = get_location(start);

	switch (start.kind) {
	case LEX_OUTPUT:
		return parse_output(state, arena, loc);
	case LEX_SYMBOL:
		return parse_stmnt_symbol(start.symbol, state, arena, loc);
	case LEX_IF:
		return parse_if(state, arena, loc, true);
	case LEX_WHILE:
		return parse_while(state, arena, loc);
	case LEX_RETURN:
		return parse_return(state, arena, loc);
	default:
		error(*loc, "Malformed statement starts with %s", lex_token_name[start.kind]);
		exit(1);
	}
}

static struct ast_top parse_subroutine(struct lex_state *state, struct mem_arena *arena, struct ast_location *loc)
{
	struct lex_token name = lex(state);
	error_assert(name.kind == LEX_SYMBOL, get_location(name), "Subroutine should have name, got %s", lex_token_name[name.kind]);
	struct lex_token tok = lex(state);
	error_assert(tok.kind == LEX_RLB, get_location(tok), "Subroutine needs argument list opened, got %s", lex_token_name[tok.kind]);

	uint32_t arg_len = 0;
	uint32_t arg_back = 16;
	uint32_t *arg_names = mem_arena_alloc(arena, arg_back, uint32_t);
	tok = lex(state);
	while (tok.kind != LEX_RRB) {
		error_assert(tok.kind == LEX_SYMBOL, get_location(tok), "Subroutine arguments should be symbols, got %s", lex_token_name[tok.kind]);
		if (arg_len >= arg_back) {
			arg_back *= 2;
			uint32_t *new_names = mem_arena_alloc(arena, arg_back, uint32_t);
			memcpy(new_names, arg_names, arg_len*sizeof(uint32_t));
			arg_names = new_names;
		}
		arg_names[arg_len] = tok.symbol;
		arg_len++;
		tok = lex(state);
		if (tok.kind == LEX_COMMA) {
			tok = lex(state);
		} else {
			error_assert(tok.kind == LEX_RRB, get_location(tok), "Subroutine argument list error unexpected %s", lex_token_name[tok.kind]);
		}
	}

	WANT_NEWLINE(state, *loc, "Subroutine start");
	struct block b = parse_block(state, arena, NULL, 0);
	parse_end(state, LEX_SUBROUTINE, name.symbol, true, "subroutine");

	return (struct ast_top) {
		.kind = AST_SUBROUTINE,
		.loc = loc,
		.proc_stmnts = b.stmnts,
		.proc_arg_names = arg_names,
		.proc_name = name.symbol,
		.proc_len = b.count,
		.proc_arg_len = arg_len
	};
}

static struct ast_top parse_function(struct lex_state *state, struct mem_arena *arena, struct ast_location *loc)
{
	struct lex_token name = lex(state);
	error_assert(name.kind == LEX_SYMBOL || op_bp[1][name.kind],
			get_location(name), "Function should have name, got %s", lex_token_name[name.kind]);
	struct lex_token tok = lex(state);
	error_assert(tok.kind == LEX_RLB, get_location(tok), "Function needs argument list opened, got %s", lex_token_name[tok.kind]);

	uint32_t arg_len = 0;
	uint32_t arg_back = 16;
	uint32_t *arg_names = mem_arena_alloc(arena, arg_back, uint32_t);
	tok = lex(state);
	while (tok.kind != LEX_RRB) {
		error_assert(tok.kind == LEX_SYMBOL, get_location(tok), "Function arguments should be symbols, got %s", lex_token_name[tok.kind]);
		if (arg_len >= arg_back) {
			arg_back *= 2;
			uint32_t *new_names = mem_arena_alloc(arena, arg_back, uint32_t);
			memcpy(new_names, arg_names, arg_len*sizeof(uint32_t));
			arg_names = new_names;
		}
		arg_names[arg_len] = tok.symbol;
		arg_len++;
		tok = lex(state);
		if (tok.kind == LEX_COMMA) {
			tok = lex(state);
		} else {
			error_assert(tok.kind == LEX_RRB, get_location(tok), "Function argument list error %s", lex_token_name[tok.kind]);
		}
	}

	WANT_NEWLINE(state, *loc, "Function start");
	struct block b = parse_block(state, arena, NULL, 0);
	if (name.kind == LEX_SYMBOL) {
		parse_end(state, LEX_FUNCTION, name.symbol, true, "function");
	} else {
		parse_end(state, LEX_FUNCTION, 0, false, "operator overload");
	}

	struct ast_top r = {
		.loc = loc,
		.proc_stmnts = b.stmnts,
		.proc_arg_names = arg_names,
		.proc_len = b.count,
		.proc_arg_len = arg_len
	};
	if (name.kind == LEX_SYMBOL) {
		r.proc_name = name.symbol;
		r.kind = AST_FUNCTION;
	} else if (name.kind == LEX_ALPHAOP) {
		r.proc_name = name.symbol;
		r.kind = AST_ALPHAOP_FUNCTION;
	} else {
		r.proc_op = operator_map[name.kind];
		r.kind = AST_OP_FUNCTION;
	}
	return r;
}

static struct ast_top parse_program(struct lex_state *state, struct mem_arena *arena, struct ast_location *loc)
{
	struct lex_token name = lex(state);
	error_assert(name.kind == LEX_SYMBOL, get_location(name), "Program should have name, got %s", lex_token_name[name.kind]);
	error_assert(lex(state).kind == LEX_NEWLINE, *loc,
			"Program first line not terminated with %s", "newline");

	struct block b = parse_block(state, arena, NULL, 0);

	parse_end(state, LEX_PROGRAM, name.symbol, true, "program");
	return (struct ast_top) {
		.kind = AST_PROGRAM,
		.prog_stmnts = b.stmnts,
		.prog_name = name.symbol,
		.prog_len = b.count,
		.loc = loc
	};
}

struct ast_top parse(struct lex_state *state, struct mem_arena *arena)
{
	struct lex_token start = skip_newlines(state);

	struct ast_location *loc = mem_arena_alloc(arena, 1, struct ast_location);
	*loc = get_location(start);

	switch (start.kind) {
	case LEX_EOF:
		return (struct ast_top) { .kind = AST_EOF };
	case LEX_PROGRAM:
		return parse_program(state, arena, loc);
	case LEX_FUNCTION:
		return parse_function(state, arena, loc);
	case LEX_SUBROUTINE:
		return parse_subroutine(state, arena, loc);
	default:
		error(*loc, "Invalid top-level thing start %s", lex_token_name[start.kind]);
		exit(1);
	}
}
