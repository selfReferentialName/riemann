#ifndef FRONTEND_H
#define FRONTEND_H

#include "util/arena.h"
#include "util/hash.h"
#include <stddef.h>
#include <stdint.h>

/// Pointers to each symbol string.
/// Note that symbol 0 is impossible.
extern char **fe_symbols;
/// Length of fe_symbols
extern size_t fe_symbol_count;
/// Backing length of fe_symbols
extern size_t fe_symbol_back;
/// Convert string into symbol
extern struct hash_table_vlk fe_symbol_table;

/// The input text to compile.
extern char *fe_text;
/// The amount of input text in bytes.
extern size_t fe_text_length;

/// Add or lookup a symbol to the symbol table
/// Allocates the symbol text on the heap if needed
uint32_t fe_add_symbol(const char *name, size_t len);

/// The symbol for TEXT
extern uint32_t fe_sym_text;
/// The symbol for INT
extern uint32_t fe_sym_int;
/// The symbol for INTEGER
extern uint32_t fe_sym_integer;
/// The symbol for LOGICAL
extern uint32_t fe_sym_logical;
/// The symbol for LOGIC
extern uint32_t fe_sym_logic;
/// The symbol for RESULT
extern uint32_t fe_sym_result;
/// The symbol for mod
extern uint32_t fe_sym_mod;

/// Run the frontend and produce IR for one top-level node
/// Returns false on EOF
bool frontend_top(struct ast_top top, struct mem_arena *arena);

/// Run the frontend and produce IR for a file
void frontend(const char *filename);

#endif
