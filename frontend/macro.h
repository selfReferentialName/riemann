#ifndef FRONTEND_MACRO_H
#define FRONTEND_MACRO_H

#include "frontend/lexer.h"
#include "frontend/ast.h"
#include "util/arena.h"
#include <stdint.h>

/// Expand a macro by putting a bunch of tokens into the next of the given lex_state
void macro_expand(uint32_t sym, uint32_t argc, uint32_t arg_len[argc], struct lex_token *args[argc],
		struct lex_state *out, struct mem_arena *arena);

/// Register a macro into the lookup table
void register_macro(struct ast_top ast, struct mem_arena *arena);

#endif
