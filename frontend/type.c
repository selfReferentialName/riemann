#include "frontend/ast.h"
#include "util/log.h"
#include "util/hash.h"
#include "util/xmalloc.h"
#include "frontend/error.h"
#include "frontend/frontend.h"

// can pass a b into an a
static bool is_compatible(struct ast_type a, struct ast_type b)
{
	switch (a.kind) {
	case TAST_TEXT:
	case TAST_INTEGER:
	case TAST_LOGIC:
		return a.kind == b.kind;
	default:
		return false;
	}
}

// procedure dispatch tree
struct proc_disp_tree {
	struct ast_type *next_type; // types of next arguments
	struct proc_disp_tree *next_tree; // corresponding node in tree by type of next argument
	size_t next_size; // number of next arguments
	size_t next_back; // backing size of next_type and next_tree
	struct ast_type last_type; // return type if this is a function with no more arguments
	uint32_t last_id; // procedure id if this has no more arguments
	bool last; // true if this can take no more arguments
};

// lookup by expression types
static void lookup_disp_tree(const struct proc_disp_tree *t, const struct ast_expr *args, size_t arg_count,
		struct ast_type *ret, uint32_t *id, const char *name, struct ast_location loc)
{
	unsigned ac0 = arg_count;
	while (arg_count) {
		for (size_t i = t->next_size - 1; i >= 0 && i != (size_t) -1; i--) {
			if (is_compatible(t->next_type[i], *args[0].type)) {
				t = &t->next_tree[i];
				goto next_arg; // continue the while loop
			}
		}
		error(*args[0].loc, "Too many arguments or bad type to call of %s, had %u, %u too many",
				name, ac0, (unsigned) arg_count);
		log_unreachable();
next_arg:
		args++;
		arg_count--;
	}

	error_assert(t->last, loc, "Too few arguments to call of %s", name);
	if (ret) *ret = t->last_type;
	*id = t->last_id;
}

static void insert_disp_tree(struct proc_disp_tree *t, const struct ast_type *args, size_t arg_count,
		struct ast_type ret, uint32_t id, const char *name, struct ast_location loc)
{
	while (arg_count) {
		for (size_t i = t->next_size - 1; i >= 0 && i != (size_t) -1; i--) {
			if (is_compatible(t->next_type[i], args[0])) {
				t = &t->next_tree[i];
				goto next_arg;
			}
		}
		// "else" (if not found)
		if (t->next_size >= t->next_back) {
			t->next_back = t->next_back ? 2*t->next_back : 4;
			t->next_type = realloc(t->next_type, t->next_back * sizeof(struct ast_type));
			t->next_tree = realloc(t->next_tree, t->next_back * sizeof(struct proc_disp_tree));
		}
		t->next_type[t->next_size] = args[0];
		t->next_tree[t->next_size] = (struct proc_disp_tree) {
			.next_back = 0
		};
		t->next_size++;
		t = t->next_tree + t->next_size - 1;
next_arg:
		args++;
		arg_count--;
	}

	error_assert(!t->last, loc, "Procedure redefinition with same types of %s", name);
	t->last = true;
	t->last_type = ret;
	t->last_id = id;
}

static const char op_name[AST_OR+1][4] = {
	"**",
	"*",
	"/",
	"\\",
	"+",
	"-",
	"==",
	"=\\=",
	"<",
	"<=",
	">=",
	">",
	"AND",
	"OR",
};

// not reset
static struct hash_table fun_table = {
	.keysize = sizeof(uint32_t),
	.valsize = sizeof(struct proc_disp_tree *)
};
static struct hash_table sub_table = {
	.keysize = sizeof(uint32_t),
	.valsize = sizeof(struct proc_disp_tree *)
};
static struct hash_table alphaop_table = {
	.keysize = sizeof(uint32_t),
	.valsize = sizeof(struct proc_disp_tree *)
};
static struct proc_disp_tree *op_table[AST_AND];

static void lookup_fun(uint32_t name, const struct ast_expr *args, size_t arg_count,
		struct ast_type *ret, uint32_t *id, struct ast_location loc)
{
	struct proc_disp_tree *t;
	error_assert(hash_table_get(fun_table, &name, &t), loc,
			"Function not found: %s", fe_symbols[name]);
	lookup_disp_tree(t, args, arg_count, ret, id, fe_symbols[name], loc);
}
static void lookup_sub(uint32_t name, const struct ast_expr *args, size_t arg_count, uint32_t *id, struct ast_location loc)
{
	struct proc_disp_tree *t;
	error_assert(hash_table_get(sub_table, &name, &t), loc,
			"Subroutine not found: %s", fe_symbols[name]);
	lookup_disp_tree(t, args, arg_count, NULL, id, fe_symbols[name], loc);
}
static void lookup_alphaop(uint32_t name, struct ast_expr lhs, struct ast_expr rhs,
		struct ast_type *ret, uint32_t *id, struct ast_location loc)
{
	struct proc_disp_tree *t;
	error_assert(hash_table_get(alphaop_table, &name, &t), loc,
			"Operator not found: .%s.", fe_symbols[name]);
	struct ast_expr args[2] = { lhs, rhs };
	lookup_disp_tree(t, args, 2, ret, id, fe_symbols[name], loc);
}
static void lookup_op(enum ast_operator op, struct ast_expr lhs, struct ast_expr rhs,
		struct ast_type *ret, uint32_t *id, struct ast_location loc)
{
	if (op == AST_AND || op == AST_OR) { // not overloadable
		error_assert(lhs.type->kind == TAST_LOGIC && rhs.type->kind == TAST_LOGIC, loc,
				"%s can only be used on LOGIC arguments", op_name[op]);
		*ret = (struct ast_type) { .kind = TAST_LOGIC };
		*id = 0;
		return;
	}
	struct proc_disp_tree *t = op_table[op];
	error_assert(t, loc, "Operator not found (probably compiler bug): %s", op_name[op]);
	struct ast_expr args[2] = { lhs, rhs };
	lookup_disp_tree(t, args, 2, ret, id, op_name[op], loc);
}

static void insert_fun(uint32_t name, const struct ast_type *args, size_t arg_count, struct ast_type ret, struct ast_location loc, uint32_t id)
{
	struct proc_disp_tree *t;
	if (!hash_table_get(fun_table, &name, &t)) {
		t = xmalloc(sizeof(struct proc_disp_tree));
		*t = (struct proc_disp_tree) { .last = false };
		hash_table_set(&fun_table, &name, &t);
	}
	insert_disp_tree(t, args, arg_count, ret, id, fe_symbols[name], loc);
}
static void insert_sub(uint32_t name, const struct ast_type *args, size_t arg_count, struct ast_location loc, uint32_t id)
{
	struct proc_disp_tree *t;
	if (!hash_table_get(sub_table, &name, &t)) {
		t = xmalloc(sizeof(struct proc_disp_tree));
		*t = (struct proc_disp_tree) { .last = false };
		hash_table_set(&sub_table, &name, &t);
	}
	struct ast_type ret = { .kind = TAST_TEXT }; // dummy
	insert_disp_tree(t, args, arg_count, ret, id, fe_symbols[name], loc);
}
static void insert_alphaop(uint32_t name, struct ast_type lhs, struct ast_type rhs, struct ast_type ret, struct ast_location loc, uint32_t id)
{
	struct proc_disp_tree *t;
	if (!hash_table_get(alphaop_table, &name, &t)) {
		t = xmalloc(sizeof(struct proc_disp_tree));
		*t = (struct proc_disp_tree) { .last = false };
		hash_table_set(&alphaop_table, &name, &t);
	}
	struct ast_type args[2] = { lhs, rhs };
	insert_disp_tree(t, args, 2, ret, id, fe_symbols[name], loc);
}
static void insert_op(enum ast_operator op, struct ast_type lhs, struct ast_type rhs, struct ast_type ret, struct ast_location loc, uint32_t id)
{
	struct proc_disp_tree *t = op_table[op];
	if (t == NULL) {
		t = xmalloc(sizeof(struct proc_disp_tree));
		*t = (struct proc_disp_tree) { .last = false };
		op_table[op] = t;
	}
	struct ast_type args[2] = { lhs, rhs };
	insert_disp_tree(t, args, 2, ret, id, op_name[op], loc);
}

// reset every top level thing
static struct hash_table local_table = {
	.keysize = sizeof(uint32_t),
	.valsize = sizeof(struct ast_type)
};

// get symbol type or return false if not declared
static bool lookup(uint32_t sym, struct ast_type *r)
{
	if (hash_table_get(local_table, &sym, r)) {
		return true;
	} else {
		return false;
	}
}

static void assert_compatible(struct ast_type a, struct ast_type b, struct ast_location loc)
{
	if (!is_compatible(a, b)) {
		error(loc, "Types incompatible with kinds %u, %u", a.kind, b.kind);
	}
}

static void type_expr(struct ast_expr *expr, struct mem_arena *arena)
{
	expr->type = mem_arena_alloc(arena, 1, struct ast_type);

	switch (expr->kind) {
	case AST_STRING:
		expr->type->kind = TAST_TEXT;
		return;
	case AST_INTEGER:
		expr->type->kind = TAST_INTEGER;
		return;
	case AST_VARIABLE:
		error_assert(lookup(expr->var, expr->type), *expr->loc,
				"Variable not found: %s", fe_symbols[expr->var]);
		return;
	case AST_LOGIC:
		expr->type->kind = TAST_LOGIC;
		return;
	case AST_FCALL:
		log_assert(expr->argc >= 1); // an invariant
		for (size_t i = 1; i < expr->argc; i++) {
			type_expr(&expr->args[i], arena);
		}
		if (expr->args[0].kind == AST_VARIABLE) {
			expr->args[0].kind = AST_FUNUSE;
			lookup_fun(expr->args[0].var, expr->args+1, expr->argc-1, expr->type, &expr->args[0].id, *expr->loc);
		} else {
			log_todo("Higher order functions");
		}
		return;
	case AST_OP:
		type_expr(expr->lhs, arena);
		type_expr(expr->rhs, arena);
		lookup_op(expr->op, *expr->lhs, *expr->rhs, expr->type, &expr->op_id, *expr->loc);
		return;
	case AST_ALPHAOP:
		type_expr(expr->lhs, arena);
		type_expr(expr->rhs, arena);
		lookup_alphaop(expr->alphaop, *expr->lhs, *expr->rhs, expr->type, &expr->op_id, *expr->loc);
		return;
	default:
		log_unreachable();
	}
}

// return true if statement is not argument declaration
static bool type_stmnt(struct ast_stmnt *stmnt, struct mem_arena *arena, struct ast_type *ret_type,
		uint32_t *arg_names, uint32_t arg_count, bool can_take_arg)
{
	struct ast_type type; // quiet valgrind?

	switch (stmnt->kind) {
	case AST_ASSIGNMENT:
		type_expr(stmnt->set_value, arena);
		if (!lookup(stmnt->set_name, &type)) { // implicit declaration
			struct ast_expr *e = stmnt->set_value;
			uint32_t n = stmnt->set_name;
			stmnt->kind = AST_DECLARATION;
			stmnt->decl_name = n;
			stmnt->decl_type = *e->type;
			stmnt->decl_init = e;
			hash_table_set(&local_table, &n, e->type);
		} else {
			assert_compatible(type, *stmnt->set_value->type, *stmnt->loc);
		}
		return true;
	case AST_DECLARATION:
		for (uint32_t i = 0; i < arg_count; i++) {
			if (arg_names[i] == stmnt->decl_name) {
				error_assert(!lookup(stmnt->decl_name, &type), *stmnt->loc,
						"Redeclaration of argument %s", fe_symbols[stmnt->decl_name]);
				error_assert(!stmnt->decl_init, *stmnt->loc,
						"Attempt to initialise argument %s", fe_symbols[stmnt->decl_name]);
				error_assert(can_take_arg, *stmnt->loc,
						"Attempt to declare argument %s in invalid location", fe_symbols[stmnt->decl_name]);
				hash_table_set(&local_table, &stmnt->decl_name, &stmnt->decl_type);
				return false;
			}
		}
		if (stmnt->decl_init) {
			type_expr(stmnt->decl_init, arena);
			assert_compatible(stmnt->decl_type, *stmnt->decl_init->type, *stmnt->loc);
		}
		error_assert(!lookup(stmnt->decl_name, &type), *stmnt->loc,
				"Redeclaration of variable %s", fe_symbols[stmnt->decl_name]);
		hash_table_set(&local_table, &stmnt->decl_name, &stmnt->decl_type);
		return true;
	case AST_SCALL:
		for (uint32_t i = 0; i < stmnt->call_len; i++) {
			type_expr(&stmnt->call_args[i], arena);
		}
		lookup_sub(stmnt->sub, stmnt->call_args, stmnt->call_len, &stmnt->sub_id, *stmnt->loc);
		return true;
	case AST_OUTPUT:
		if (stmnt->format) type_expr(stmnt->format, arena);
		for (uint32_t i = 0; i < stmnt->io_len; i++) {
			type_expr(&stmnt->io_args[i], arena);
		}
		return true;
	case AST_RETURN:
		if (ret_type) {
			error_assert(stmnt->e, *stmnt->loc,
					"Attempt to return from function without %s", "value");
			type_expr(stmnt->e, arena);
			error_assert(ret_type != (struct ast_type *) 1, *stmnt->loc,
					"Attempt to return from function before return type %s", "known");
			assert_compatible(*ret_type, *stmnt->e->type, *stmnt->loc);
		} else {
			error_assert(stmnt->e == NULL, *stmnt->loc,
					"Attempt to return value from %s", "subroutine or program");
		}
		return true;
	case AST_IF:
		type_expr(stmnt->if_cond, arena);
		assert_compatible((struct ast_type) { .kind = TAST_LOGIC }, *stmnt->if_cond->type, *stmnt->if_cond->loc);
		for (uint32_t i = 0; i < stmnt->if_else_len; i++) {
			type_stmnt(&stmnt->if_else[i], arena, ret_type, arg_names, arg_count, false);
		}
		for (uint32_t i = 0 ; i < stmnt->block_len; i++) {
			type_stmnt(&stmnt->stmnts[i], arena, ret_type, arg_names, arg_count, false);
		}
		return true;
	case AST_WHILE:
		type_expr(stmnt->while_cond, arena);
		assert_compatible((struct ast_type) { .kind = TAST_LOGIC }, *stmnt->while_cond->type, *stmnt->while_cond->loc);
		for (uint32_t i = 0 ; i < stmnt->block_len; i++) {
			type_stmnt(&stmnt->stmnts[i], arena, ret_type, arg_names, arg_count, false);
		}
		return true;
	default:
		log_unreachable();
	}
}

static void insert_alphaop(uint32_t name, struct ast_type lhs, struct ast_type rhs, struct ast_type ret, struct ast_location loc, uint32_t id);
static void insert_op(enum ast_operator op, struct ast_type lhs, struct ast_type rhs, struct ast_type ret, struct ast_location loc, uint32_t id);

static void init(void)
{
	[[maybe_unused]] struct ast_type text = { .kind = TAST_TEXT };
	[[maybe_unused]] struct ast_type integer = { .kind = TAST_INTEGER };
	[[maybe_unused]] struct ast_type logic = { .kind = TAST_LOGIC };
	[[maybe_unused]] struct ast_location loc = { .file = "<<builtin>>" };

	insert_op(AST_TIMES, integer, integer, integer, loc, 0);
	insert_op(AST_RDIV, integer, integer, integer, loc, 0);
	insert_op(AST_LDIV, integer, integer, integer, loc, 0);
	insert_op(AST_ADD, integer, integer, integer, loc, 0);
	insert_op(AST_SUB, integer, integer, integer, loc, 0);
	insert_op(AST_EQ, integer, integer, logic, loc, 0);
	insert_op(AST_NEQ, integer, integer, logic, loc, 0);
	insert_op(AST_LT, integer, integer, logic, loc, 0);
	insert_op(AST_LE, integer, integer, logic, loc, 0);
	insert_op(AST_GE, integer, integer, logic, loc, 0);
	insert_op(AST_GT, integer, integer, logic, loc, 0);
	insert_alphaop(fe_sym_mod, integer, integer, integer, loc, 0);

	insert_op(AST_EQ, logic, logic, logic, loc, 0);
	insert_op(AST_NEQ, logic, logic, logic, loc, 0);
}

void type_ast(struct ast_top *top, struct mem_arena *arena)
{
	static bool initialised;

	if (!initialised) {
		init();
		initialised = true;
	}

	static uint32_t next_id;
	bool has_fun_type = false;
	bool arg_decl_done = false; // argument declerations done

	switch (top->kind) {
	case AST_PROGRAM:
		for (uint32_t i = 0; i < top->prog_len; i++) {
			type_stmnt(&top->prog_stmnts[i], arena, NULL, NULL, 0, false);
		}
		hash_table_clean(&local_table);
		return;
	case AST_OP_FUNCTION:
		error_assert(top->proc_op != AST_AND && top->proc_op != AST_OR, *top->loc,
				"Cannot overload %s", op_name[top->proc_op]);
		[[fallthrough]];
	case AST_FUNCTION:
	case AST_ALPHAOP_FUNCTION:
		top->proc_id = next_id;
		top->proc_arg_types = mem_arena_alloc(arena, top->proc_arg_len, struct ast_type);
		for (uint32_t i = 0; i < top->proc_len; i++) {
			if (!has_fun_type) {
				if (top->kind == AST_OP_FUNCTION) {
					has_fun_type = lookup(fe_sym_result, &top->fun_type);
				} else {
					has_fun_type = lookup(top->proc_name, &top->fun_type);
				}
			}
			// bitwise or to avoid short circut
			arg_decl_done = arg_decl_done | type_stmnt(&top->proc_stmnts[i], arena,
					has_fun_type ? &top->fun_type : (struct ast_type *) 1,
					top->proc_arg_names, top->proc_arg_len, !arg_decl_done);
			if (!arg_decl_done) {
				top->proc_stmnts++;
				top->proc_len--;
				i--;
			}
		}
		for (uint32_t i = 0; i < top->proc_arg_len; i++) {
			error_assert(lookup(top->proc_arg_names[i], top->proc_arg_types+i), *top->loc,
					"%s %s argument %s has no known type",
					top->kind == AST_FUNCTION ? "Function" : "Operator",
					top->kind == AST_OP_FUNCTION ? op_name[top->proc_op] : fe_symbols[top->proc_name],
					top->proc_arg_names[i]);
		}
		error_assert(has_fun_type || lookup(top->proc_name, &top->fun_type), *top->loc,
				"%s %s not given type",
				top->kind == AST_FUNCTION ? "Function" : "Operator",
				top->kind == AST_OP_FUNCTION ? op_name[top->proc_op] : fe_symbols[top->proc_name]);
		if (top->kind == AST_FUNCTION) {
			insert_fun(top->proc_name, top->proc_arg_types, top->proc_arg_len, top->fun_type, *top->loc, next_id);
		} else if (top->kind == AST_OP_FUNCTION) {
			error_assert(top->proc_arg_len == 2, *top->loc,
					"Operator %s has wrong number of arguments", op_name[top->proc_op]);
			insert_op(top->proc_op, top->proc_arg_types[0], top->proc_arg_types[1], top->fun_type, *top->loc, next_id);
		} else {
			error_assert(top->proc_arg_len == 2, *top->loc,
					"Operator %s has wrong number of arguments", fe_symbols[top->proc_name]);
			insert_alphaop(top->proc_name, top->proc_arg_types[0], top->proc_arg_types[1], top->fun_type, *top->loc, next_id);
		}
		next_id++;
		hash_table_clean(&local_table);
		return;
	case AST_SUBROUTINE:
		top->proc_id = next_id;
		top->proc_arg_types = mem_arena_alloc(arena, top->proc_arg_len, struct ast_type);
		for (uint32_t i = 0; i < top->proc_len; i++) {
			// bitwise or to avoid short circut
			arg_decl_done = arg_decl_done | type_stmnt(&top->proc_stmnts[i], arena,
					NULL, top->proc_arg_names, top->proc_arg_len, !arg_decl_done);
			if (!arg_decl_done) {
				top->proc_stmnts++;
				top->proc_len--;
				i--;
			}
		}
		for (uint32_t i = 0; i < top->proc_arg_len; i++) {
			error_assert(lookup(top->proc_arg_names[i], top->proc_arg_types+i), *top->loc,
					"Subroutine %s argument %u has no known type",
					fe_symbols[top->proc_name], i);
		}
		insert_sub(top->proc_name, top->proc_arg_types, top->proc_arg_len, *top->loc, next_id);
		next_id++;
		hash_table_clean(&local_table);
		return;
	case AST_EOF:
		return;
	default:
		log_unreachable();
	}
}
