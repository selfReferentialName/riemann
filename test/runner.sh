#!/bin/sh

set -e

./rmnc $1 $1.ll
clang $1.ll runtime.a -o $1.a.out
./$1.a.out
rm $1.ll $1.a.out
