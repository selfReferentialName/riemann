# User changeable configuration.
CC=clang
LD=clang
CFLAGS=-std=c2x -O2 -Wall -Werror -Wno-error=missing-braces -g -Og -Wfatal-errors -Wno-error=unused-function -Wno-constant-logical-operand
CFLAGS+=-gdwarf-4
#CFLAGS+=-fprofile-arcs -ftest-coverage

# Update configuration to include dependencies and stuff
LFLAGS:=$(CFLAGS)
CFLAGS+=-I.

.MAIN: rmnc runtime.a

OBJECTS=compiler/compiler.o \
	compiler/llvm/compile.o compiler/llvm/template-amd64-linux.o \
	core/ir.o core/disasm.o core/calc-preds.o core/intern.o \
	frontend/frontend.o frontend/lexer.o frontend/lower.o frontend/parse.o \
	frontend/type.o frontend/error.o \
	util/arena.o util/array.o util/hash.o util/log.o util/xmalloc.o \
	util/escape.o \
	rmnc.o cl.o
RESOURCES=
INTERMEDIATES=

RUNTIME_OBJECTS=runtime/output.o

.SUFFIXES: .o

rmnc: $(OBJECTS)
	$(LD) $(OBJECTS) $(LFLAGS) -o rmnc

runtime.a: $(RUNTIME_OBJECTS)
	rm -f runtime.a
	ar qc runtime.a $(RUNTIME_OBJECTS)

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<

compiler/llvm/compile.o: compiler/llvm/compile.c compiler/llvm/header.ll
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY: cloc docs clean check $(TESTS)

check:	rmnc runtime.a
	turnt test/*.rm

cloc:
	cloc --exclude-dir=docs .

docs:
	doxygen doxygen.config
	make -C docs/latex

clean:
	rm -f $(OBJECTS)
	rm -f $(RESOURCES)
	rm -f $(INTERMEDIATES)

check: $(TESTS)
