#include "core/ir.h"
#include "compiler/llvm/template.h"
#include "util/log.h"
#include "util/xmalloc.h"
#include "util/escape.h"
#include <stdio.h>

// I can dream
#ifdef HAVE_EMBED
static const char header[] = {
#embed "compile/llvm/header.ll"
};
#else
// this is obviously worse why do people genuinely think this is better
static const char header[] = {
	0x0a, 0x64, 0x65, 0x63, 0x6c, 0x61, 0x72, 0x65, 0x20, 0x76, 0x6f, 0x69,
	0x64, 0x20, 0x40, 0x6f, 0x75, 0x74, 0x70, 0x75, 0x74, 0x5f, 0x73, 0x74,
	0x64, 0x6f, 0x75, 0x74, 0x28, 0x69, 0x38, 0x2a, 0x2c,
	0x20, 0x2e, 0x2e, 0x2e, 0x29, 0x0a, 0x0a
};
#endif

static struct ir_type *var_type;
static uint32_t var_type_back;

static void set_var_type(uint32_t var, struct ir_type type)
{
	if (var >= var_type_back) {
		var_type_back = var*2;
		var_type = xrealloc(var_type, var_type_back*sizeof(struct ir_type));
	}
	var_type[var] = type;
}

static void type_fun(struct ir_fun fun)
{
	for (uint32_t i = 0; i < fun.arg_count; i++) {
		set_var_type(i + 1, fun.args[i]);
	}
	for (uint32_t i = 0; i < fun.bb_count; i++) {
		for (uint32_t j = 0; j < fun.bb[i].arg_count; i++) {
			set_var_type(fun.bb[i].args[j], fun.bb[i].arg_types[j]);
		}
		for (uint32_t j = 0; j < fun.bb[i].inst_count; j++) {
			if (fun.bb[i].insts[j].out) {
				set_var_type(fun.bb[i].insts[j].out, fun.bb[i].insts[j].type);
			}
		}
	}
}

static void compile_type(FILE *file, struct ir_type type)
{
	switch (type.kind) {
	case IR_VOID:
		fputs("void", file);
		break;
	case IR_TEXT:
		fputs("i8*", file);
		break;
	case IR_INT:
		fputs("i64", file);
		break;
	case IR_LOGIC:
		fputs("i1", file);
		break;
	case IR_LINPTR:
		compile_type(file, *type.arg);
		fputc('*', file);
		break;
	}
}

static void compile_global(FILE *file, struct ir_global global)
{
	char escape_tmp[256];
	size_t escape_len; // proper C23 when

	fprintf(file, "@%s = internal global ", global.sym);
	if (global.type.kind == IR_TEXT) {
		fprintf(file, "[ %llu x i8 ]", (unsigned long long) global.init.len);
	} else {
		compile_type(file, global.type);
	}
	fputc(' ', file);
	switch (global.type.kind) {
	case IR_VOID:
	case IR_LINPTR:
		log_unreachable();
	case IR_TEXT:
		escape_len = escape(global.init.data, escape_tmp, sizeof(escape_tmp));
		if (escape_len) {
			char *tmp = xmalloc(escape_len);
			log_assert(!escape(global.init.data, tmp, escape_len));
			fprintf(file, "c\"%s\\00\"", tmp);
			free(tmp);
		} else {
			fprintf(file, "c\"%s\\00\"", escape_tmp);
		}
		break;
	case IR_INT:
		fprintf(file, "%llu", (long long unsigned) global.init.intval);
		break;
	case IR_LOGIC:
		fputs(global.init.logicval ? "true" : "false", file);
		break;
	}
	fputc('\n', file);
}

static const char op_name[IR_OR+1][9] = {
	//12345678
	"mul nsw",
	"sdiv",
	"ohno!!!",
	"add nsw",
	"sub nsw",
	"icmp eq",
	"icmp ne",
	"icmp slt",
	"icmp sle",
	"icmp sge",
	"icmp sgt",
	"and",
	"or",
};

static void compile_inst(FILE *file, struct ir_inst inst, uint32_t bb)
{
	static uint32_t next_var; // in case we need to break up instructions
	uint32_t tmp; // Clang actual C23 support when

	if (inst.kind == IR_OP && inst.op == IR_MOD) { // need to absolute value
		uint32_t rem = next_var++;
		fprintf(file, "%%%u = srem ", rem);
		compile_type(file, inst.type);
		fprintf(file, " %%v%u, %%v%u\n\t", inst.lhs, inst.rhs);
		uint32_t neg = next_var++;
		fprintf(file, "%%%u = icmp slt ", neg);
		compile_type(file, inst.type);
		fprintf(file, "%%%u, 0\n\t", rem);
		uint32_t ifneg = next_var++;
		uint32_t pos = next_var++;
		uint32_t cont = next_var++;
		fprintf(file, "br i1 %%%u, label %%%u, label %%%u\n", neg, ifneg, cont);
		fprintf(file, "%u:\n\t", ifneg);
		fprintf(file, "%%%u = sub ", pos);
		compile_type(file, inst.type);
		fprintf(file, " 0, %%%u\n\t", rem);
		fprintf(file, "br label %%%u\n", cont);
		fprintf(file, "%u:\n\t", cont);
		fprintf(file, "%%v%u = phi ", inst.out);
		compile_type(file, inst.type);
		fprintf(file, " [ %%%u, %%%u ], [ %%%u, %%b%u ]", pos, ifneg, rem, bb);
		return;
	}

	if (inst.out) fprintf(file, "%%v%u = ", inst.out);
	switch (inst.kind) {
	case IR_DECLARE:
		fputs("alloca ", file);
		compile_type(file, *inst.type.arg);
		break;
	case IR_STORE:
		fputs("store ", file);
		compile_type(file, var_type[inst.argn[1]]);
		fprintf(file, " %%v%u, ", inst.argn[1]);
		compile_type(file, var_type[inst.argn[0]]);
		fprintf(file, " %%v%u", inst.argn[0]);
		break;
	case IR_LOAD:
		fputs("load ", file);
		compile_type(file, inst.type);
		fputs(", ", file);
		compile_type(file, inst.type);
		fprintf(file, "* %%v%u", inst.arg);
		break;
	case IR_STORE_GLOBAL:
		// TODO: store to TEXT
		fputs("store ", file);
		compile_type(file, var_type[inst.argn[1]]);
		fprintf(file, " %%v%u, ", inst.argn[1]);
		compile_type(file, var_type[inst.argn[1]]);
		fprintf(file, "* @%s", ir_globals[inst.argn[0]].sym);
		break;
	case IR_LOAD_GLOBAL:
		if (inst.type.kind == IR_TEXT) {
			fprintf(file, "bitcast [ %llu x i8 ]* @%s to i8*",
					(unsigned long long) ir_globals[inst.arg].init.len,
					ir_globals[inst.arg].sym);
		} else {
			fputs("load ", file);
			compile_type(file, inst.type);
			fputs(", ", file);
			compile_type(file, inst.type);
			fprintf(file, "* @%s", ir_globals[inst.arg].sym);
		}
		break;
	case IR_CONST_STRING:
		log_unreachable(); // removed by ir_intern
	case IR_CONST_INT:
		fprintf(file, "bitcast i64 %llu to i64", (long long unsigned) inst.intval); // NOP with immediate
		break;
	case IR_CONST_LOGIC:
		fprintf(file, "bitcast i1 %s to i1", inst.logicval ? "true" : "false");
		break;
	case IR_CALL:
		fputs("call fastcc ", file);
		if (inst.out) {
			compile_type(file, inst.type);
		} else {
			fputs("void", file);
		}
		fprintf(file, " @%s(", ir_funs[inst.call_fun].sym);
		for (uint32_t i = 0; i < inst.call_arg_count; i++) {
			if (i != 0) fputs(", ", file);
			compile_type(file, var_type[inst.call_args[i]]);
			fprintf(file, " %%v%u", inst.call_args[i]);
		}
		fputs(")", file);
		break;
	case IR_OUTPUT:
		// why won't LLVM zext this for me?
		tmp = next_var;
		for (uint32_t i = 0; i < inst.arg_count; i++) {
			if (var_type[inst.args[i]].kind == IR_LOGIC) {
				fprintf(file, "%%%u = zext i1 %%v%u to i32\n\t",
						next_var, inst.args[i]);
				next_var++;
			}
		}
		fputs("call void (i8*, ...) ", file);
		fputs("@output_stdout(", file);
		for (uint32_t i = 0; i < inst.arg_count; i++) {
			if (i != 0) fputs(", ", file);
			if (var_type[inst.args[i]].kind == IR_LOGIC) {
				fprintf(file, "i32 %%%u", tmp);
				tmp++;
			} else {
				compile_type(file, var_type[inst.args[i]]);
				fprintf(file, " %%v%u", inst.args[i]);
			}
		}
		fputs(")", file);
		break;
	case IR_OP: // not IR_MOD
		fputs(op_name[inst.op], file);
		fputc(' ', file);
		compile_type(file, var_type[inst.lhs]);
		fprintf(file, " %%v%u, %%v%u", inst.lhs, inst.rhs);
		break;
	case IR_NOT:
		fprintf(file, "xor i1 %%v%u, 1", inst.arg);
		break;
	case IR_NEG:
		fprintf(file, "sub nsw 0, %%v%u", inst.arg);
		break;
	}
}

static void compile_bb(FILE *file, struct ir_bb bb, struct ir_fun fun, uint32_t idx)
{
	// convert branch arguments into phi nodes
	for (uint32_t i = 0; i < bb.arg_count; i++) {
		fprintf(file, "\t%%v%u = phi ", bb.args[i]);
		compile_type(file, bb.arg_types[i]);
		fputc(' ', file);
		for (uint32_t j = 0; j < bb.pred_count; j++) {
			if (j != 0) fputs(", ", file);
			struct ir_term from = fun.bb[bb.preds[j]].term;
			switch (from.kind) {
			case IR_RET:
				log_unreachable();
			case IR_GOTO:
				fprintf(file, "[ %%v%u, %%b%u ]", from.args[i], bb.preds[j]);
				break;
			case IR_BRANCH:
				fprintf(file, "[ %%v%u, %%b%u ]",
						from.br_then==idx ? from.br_then_args[i] : from.br_else_args[i], bb.preds[j]);
				break;
			}
		}
		fputc('\n', file);
	}

	for (uint32_t i = 0; i < bb.inst_count; i++) {
		fputc('\t', file);
		compile_inst(file, bb.insts[i], idx);
		fputc('\n', file);
	}

	fputc('\t', file);
	switch (bb.term.kind) {
	case IR_RET:
		fputs("ret ", file);
		if (bb.term.ret_value) {
			compile_type(file, fun.type);
			fprintf(file, " %%v%u", bb.term.ret_value);
		} else {
			fputs("void", file);
		}
		break;
	case IR_GOTO:
		fprintf(file, "br label %%b%u", bb.term.target);
		break;
	case IR_BRANCH:
		fprintf(file, "br i1 %%v%u, label %%b%u, label %%b%u",
				bb.term.br_cond, bb.term.br_then, bb.term.br_else);
		break;
	}
	fputc('\n', file);
}

static void compile_fun(FILE *file, struct ir_fun fun)
{
	ir_calc_fun_preds(&fun);
	type_fun(fun);
	fprintf(file, "define %s ", "internal fastcc");
	compile_type(file, fun.type);
	fprintf(file, "@%s (", fun.sym);
	for (uint32_t i = 0; i < fun.arg_count; i++) {
		if (i != 0) fputs(", ", file);
		compile_type(file, fun.args[i]);
		fprintf(file, " %%v%u", i + 1);
	}
	fprintf(file, ") {\n");
	for (uint32_t i = 0; i < fun.bb_count; i++) {
		fprintf(file, "b%u:\n", i);
		compile_bb(file, fun.bb[i], fun, i);
	}
	fputs("}\n\n", file);
}

static void compile_main(FILE *file)
{
	if (ir_entry_count == 0) {
		return;
	} else if (ir_entry_count == 1) {
		fputs("define i32 @main(i32 %argc, i8** %argv) {\n", file);
		fprintf(file, "\tcall void @%s()\n", ir_funs[ir_entries[0].fun].sym);
		fputs("\tret i32 0\n}\n\n", file);
	} else {
		fputs("define i32 @main(i32 %argc, i8** %argv) {\n", file);
		log_todo("Program selection");
		fputs("\tret i32 0\n}\n\n", file);
	}
}

void compile_llvm(FILE *file, const char *source)
{
	ir_intern();
	fprintf(file, "source_filename = \"%s\"\n", source);
	fprintf(file, "target datalayout = \"%s\"\n", llvm_datalayout);
	fprintf(file, "target triple = \"%s\"\n", llvm_triple);
	fwrite(header, 1, sizeof(header), file);
	for (uint32_t i = 0; i < ir_global_count; i++) {
		compile_global(file, ir_globals[i]);
	}
	fputc('\n', file);
	for (uint32_t i = 0; i < ir_fun_count; i++) {
		type_fun(ir_funs[i]);
		compile_fun(file, ir_funs[i]);
	}
	compile_main(file);
}
