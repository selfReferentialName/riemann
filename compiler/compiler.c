#include "compiler/compiler.h"
#include "util/log.h"
#include <stdio.h>

void compile_llvm(FILE *file, const char * infile);

void compile(const char *filename, enum cg_target backend, const char *infile)
{
	FILE *file = fopen(filename, "w");
	log_assert(file);

	switch (backend) {
	case CG_TARGET_LLVM:
		compile_llvm(file, infile);
		break;
	default:
		log_unreachable();
	}

	log_assert(!(fclose(file)));
}
