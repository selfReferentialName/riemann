#ifndef COMPILER_H
#define COMPILER_H

#include "core/ir.h"

/// What output format to write the IR to
enum cg_target {
	CG_TARGET_LLVM, //< Write LLVM assembly
};

/// Write the compiled program to the given file
void compile(const char *filename, enum cg_target backend, const char *infile);

#endif
