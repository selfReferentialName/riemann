#include "core/ir.h"
#include "util/xmalloc.h"
#include <stdlib.h>

static uint32_t *backs; // backing sizes of bb.preds
static uint32_t back_back; // backing size of backs

static void add_pred(struct ir_fun *fun, uint32_t b, uint32_t pred)
{
	if (fun->bb[b].pred_count >= backs[b]) {
		backs[b] = backs[b] ? backs[b]*2 : 8;
		fun->bb[b].preds = xrealloc(fun->bb[b].preds, backs[b]*sizeof(uint32_t));
	}
	fun->bb[b].preds[fun->bb[b].pred_count] = pred;
	fun->bb[b].pred_count++;
}

void ir_calc_fun_preds(struct ir_fun *fun)
{
	if (fun->bb[0].preds) return; // nothing to do

	if (back_back < fun->bb_count) {
		back_back = fun->bb_count;
		if (backs) free(backs);
		backs = xcalloc(back_back, sizeof(uint32_t));
	} else {
		for (uint32_t i = 0; i < fun->bb_count; i++) {
			backs[i] = 0;
		}
	}

	for (uint32_t i = 0; i < fun->bb_count; i++) {
		switch (fun->bb[i].term.kind) {
		case IR_RET:
			break;
		case IR_GOTO:
			add_pred(fun, fun->bb[i].term.target, i);
			break;
		case IR_BRANCH:
			add_pred(fun, fun->bb[i].term.br_then, i);
			add_pred(fun, fun->bb[i].term.br_else, i);
			break;
		}
	}
}

void ir_calc_preds(void)
{
	for (size_t i = 0; i < ir_fun_count; i++) {
		ir_calc_fun_preds(ir_funs+i);
	}
}
