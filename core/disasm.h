#ifndef CORE_DISASM_H
#define CORE_DISASM_H

#include <stdio.h>

/// Dump an assembly representation of the IR to a file
void core_disasm(FILE *f);

#endif
