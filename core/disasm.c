#include "core/disasm.h"
#include "core/ir.h"
#include "util/log.h"
#include "util/escape.h"
#include "util/xmalloc.h"

// currently don't disassemble locations
// whatever those are

static void disasm_type(FILE *file, struct ir_type type)
{
	switch (type.kind) {
	case IR_VOID:
		fputs("VOID", file);
		break;
	case IR_TEXT:
		fputs("TEXT", file);
		break;
	case IR_INT:
		fputs("INT", file);
		break;
	case IR_LOGIC:
		fputs("LOGIC", file);
		break;
	case IR_LINPTR:
		fputs("L*(", file);
		disasm_type(file, *type.arg);
		fputs(")", file);
		break;
	default:
		log_unreachable();
	}
}

static void disasm_global(FILE *file, struct ir_global global)
{
	fprintf(file, ".global %s : ", global.sym);
	disasm_type(file, global.type);
	fputs(" = ", file);
	switch (global.type.kind) {
	case IR_VOID:
	case IR_LINPTR:
		log_unreachable();
	case IR_TEXT:
		{
		size_t len = escape(global.init.data, NULL, 0);
		char *tmp = xmalloc(len);
		log_assert(!escape(global.init.data, tmp, len));
		fprintf(file, "\"%s\"", tmp);
		free(tmp);
		}
		break;
	case IR_INT:
		fprintf(file, "%lld", (long long) global.init.intval);
		break;
	case IR_LOGIC:
		fputs(global.init.logicval ? "true" : "false", file);
		break;
	}
}

static const char op_name[IR_OR+1][4] = {
	"MUL",
	"DIV",
	"ADD",
	"SUB",
	"EQ",
	"NE",
	"LT",
	"LE",
	"GE",
	"GT",
	"AND",
	"OR",
};

static void disasm_inst(FILE *file, struct ir_inst inst)
{
	switch (inst.kind) {
	case IR_DECLARE:
		fprintf(file, "%u = DECLARE ", inst.out);
		disasm_type(file, *inst.type.arg);
		break;
	case IR_STORE:
		fprintf(file, "STORE %u, %u", inst.argn[0], inst.argn[1]);
		break;
	case IR_LOAD:
		fprintf(file, "%u = LOAD %u", inst.out, inst.arg);
		break;
	case IR_STORE_GLOBAL:
		fprintf(file, "STORE_GLOBAL %s, %u", ir_globals[inst.argn[0]].sym, inst.argn[1]);
		break;
	case IR_LOAD_GLOBAL:
		fprintf(file, "%u = LOAD_GLOBAL %s", inst.out, ir_globals[inst.argn[0]].sym);
		break;
	case IR_CONST_STRING:
		// TODO: escape
		fprintf(file, "%u = CONST_STRING \"%s\"", inst.out, inst.string);
		break;
	case IR_CONST_INT:
		fprintf(file, "%u = CONST_INT %lld", inst.out, (long long int) inst.intval);
		break;
	case IR_CONST_LOGIC:
		fprintf(file, "%u = CONST_LOGIC %c", inst.out, inst.logicval ? 't' : 'f');
		break;
	case IR_CALL:
		if (inst.out) {
			fprintf(file, "%u = ", inst.out);
		}
		fprintf(file, "CALL %s", ir_funs[inst.call_fun].sym);
		for (uint32_t i = 0; i < inst.call_arg_count; i++) {
			fprintf(file, ", %u", inst.args[i]);
		}
		break;
	case IR_OUTPUT:
		fprintf(file, "OUTPUT[%u]", inst.arg_count);
		for (int i = 0; i < inst.arg_count; i++) {
			fprintf(file, "%s %u", i==0?"":",", inst.args[i]);
		}
		break;
	case IR_OP:
		fprintf(file, "%u = %s %u, %u", inst.out, op_name[inst.op], inst.lhs, inst.rhs);
		break;
	default:
		log_f(LOG_ALWAYS, "Unrecognised instruction %d", (int) inst.kind);
		log_unreachable();
	}
}

static void disasm_bb(FILE *file, struct ir_bb bb)
{
	for (int i = 0; i < bb.inst_count; i++) {
		fputs("\t", file);
		disasm_inst(file, bb.insts[i]);
		fputs("\n", file);
	}
	switch (bb.term.kind) {
	case IR_RET:
		if (bb.term.ret_value) {
			fprintf(file, "\tRET %u", bb.term.ret_value);
		} else {
			fputs("\tRET", file);
		}
		break;
	case IR_GOTO:
		fprintf(file, "\tGOTO b%u", bb.term.target);
		break;
	case IR_BRANCH:
		fprintf(file, "\tBRANCH %u, b%u, b%u", bb.term.br_cond, bb.term.br_then, bb.term.br_else);
		break;
	}
	for (uint32_t i = 0; i < bb.term.arg_count; i++) {
		fprintf(file, ", %u", bb.term.args[i]);
	}
	fputs("\n", file);
}

static void disasm_fun(FILE *file, struct ir_fun fun)
{
	fprintf(file, ".fun %s\n", fun.sym);
	fprintf(file, ".args %u", (unsigned int) fun.arg_count);
	for (int i = 0; i < fun.arg_count; i++) {
		fputs(" ", file);
		disasm_type(file, fun.args[i]);
	}
	for (unsigned i = 0; i < fun.bb_count; i++) {
		fprintf(file, "\n.bb %u\n", i);
		disasm_bb(file, fun.bb[i]);
		fputs(".endbb", file);
	}
	fputs("\n.endfun\n", file);
}

void core_disasm(FILE *file)
{
	for (size_t i = 0; i < ir_entry_count; i++) {
		fprintf(file, ".entry %s\t%s\n", ir_entries[i].name, ir_funs[ir_entries[i].fun].sym);
	}
	fputs("\n", file);
	for (uint32_t i = 0; i < ir_global_count; i++) {
		disasm_global(file, ir_globals[i]);
		fputs("\n", file);
	}
	fputs("\n", file);
	for (size_t i = 0; i < ir_fun_count; i++) {
		disasm_fun(file, ir_funs[i]);
		fputs("\n", file);
	}
}
