#include "core/ir.h"
#include "util/array.h"
#include "util/xmalloc.h"
#include <string.h>
#include <stdio.h>

// TODO: actually deduplicate

struct ir_global *ir_globals;
uint32_t ir_global_count;
uint32_t ir_global_back;

static char *gensym(void)
{
	static unsigned idx;
	char *r = xmalloc(16);
	snprintf(r, 16, "_c%u", idx);
	r[15] = 0;
	idx++;
	return r;
}

static void intern_bb(struct ir_bb *bb)
{
	struct ir_global global;
	uint32_t idx;
	for (uint32_t i = 0; i < bb->inst_count; i++) {
		switch (bb->insts[i].kind) {
		case IR_CONST_STRING:
			global = (struct ir_global) {
				.sym = gensym(),
				.type = {
					.kind = IR_TEXT
				},
				.init = {
					.data = bb->insts[i].string,
					.len = bb->insts[i].string_len
				}
			};
			idx = ir_global_count;
			array_insert32((void **) &ir_globals, &ir_global_count, &ir_global_back, &global, sizeof(struct ir_global));
			bb->insts[i] = (struct ir_inst) {
				.kind = IR_LOAD_GLOBAL,
				.out = bb->insts[i].out,
				.type = { .kind = IR_TEXT },
				.arg = idx
			};
			break;
		default:
			break;
		}
	}
}

void ir_intern(void)
{
	for (size_t i = 0; i < ir_fun_count; i++) {
		for (uint32_t j = 0; j < ir_funs[i].bb_count; j++) {
			intern_bb(&ir_funs[i].bb[j]);
		}
	}
}
