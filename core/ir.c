#include "core/ir.h"

struct ir_fun *ir_funs;
size_t ir_fun_count;
size_t ir_fun_back;

struct ir_entry *ir_entries;
size_t ir_entry_count;
size_t ir_entry_back;
