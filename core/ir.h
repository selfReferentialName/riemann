/** @file core/ir.h
 *
 * The core intermediate language.
 *
 * All things here that need to be malloc'ed are not to
 * be referenced anywhere else between passes.
 */
#ifndef CORE_IR_H
#define CORE_IR_H

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

/// A location for debugging
struct ir_loc {
	const char *file; //< The name of the source file
	uint32_t line; //< The source line
};

/// What kind of type a type is (e.g. array, integer)
enum ir_type_kind {
	IR_VOID, //< No type, null terminator
	IR_TEXT, //< A string
	IR_INT, //< An integer
	IR_LOGIC, //< A logical
	IR_LINPTR, //< A linear pointer which can't be copied (except as a GOTO argument)
};

/// A type and all its information
struct ir_type {
	enum ir_type_kind kind; //< What kind of type
	union {
		struct ir_type *arg; //< Argument for type -> type kinds e.g. pointers
	};
};

/// A global variable
struct ir_global {
	char *sym; //< What to put in the symbol table
	struct ir_type type; //< The type of the variable
	union { //< Initial value
		struct {
			void *data; //< Generic variable size constant (e.g. string litteral)
			size_t len; //< Number of bytes of a variable size constant
		};
		int64_t intval; //< Integer value
		bool logicval; //< LOGIC value
	} init;
};

/// All global variables
extern struct ir_global *ir_globals;
/// Number of global variables
extern uint32_t ir_global_count;
/// Backing size of ir_globals
extern uint32_t ir_global_back;

/// All builtin binary operations
enum ir_op {
	IR_MUL, //< x = MUL lhs, rhs (signed integer multiply)
	IR_DIV, //< x = DIV lhs, rhs (signed integer division with truncation)
	IR_MOD, //< x = MOD lhs, rhs (always positive remainder)
	IR_ADD, //< x = ADD lhs, rhs (integer addition)
	IR_SUB, //< x = SUB lhs, rhs (integer subtraction)
	IR_EQ, //< x = EQ lhs, rhs (integer and logical equality)
	IR_NE, //< x = NE lhs, rhs (integer and logical inequality)
	IR_LT, //< x = LT lhs, rhs (integer less than)
	IR_LE, //< x = LE lhs, rhs (integer less than or equal to)
	IR_GE, //< x = GE lhs, rhs (integer greater than or equal to)
	IR_GT, //< x = GT lhs, rhs (integer greater than)
	IR_AND, //< x = AND lhs, rhs (bitwise and logical and)
	IR_OR, //< x = OR lhs, rhs (bitwise and logical or)
};

/// All kinds of instructions
enum ir_inst_kind {
	IR_DECLARE, //< x = DECLARE type (making a new variable as a pointer)
	IR_STORE, //< STORE p, v (store a value into a pointer, pointer first unlike LLVM)
	IR_LOAD, //< x = LOAD p (load a value from a pointer)
	IR_STORE_GLOBAL, //< STORE_GLOBAL g, v (store a value into a global)
	IR_LOAD_GLOBAL, //< LOAD_GLOBAL g (load a value from a global)
	IR_CONST_STRING, //< x = CONST_STRING "str" (string litterals)
	IR_CONST_INT, //< x = CONST_INT int (integer litterals)
	IR_CONST_LOGIC, //< x = CONST_LOGIC t or x = CONST_LOGIC f (logical litterals)
	IR_CALL, //< [x =] CALL f, a1, a2, ... (call fun with args)
	IR_NOT, //< x = NOT y (logical negation)
	IR_NEG, //< x = NEG y (arithmatic negation)
	IR_OP, //< x = op lhs, rhs (builtin binary operation)
	IR_OUTPUT, //< OUTPUT f, x, ... (formatted output with newline)
};
/// An instruction and arguments
struct ir_inst {
	union {
		struct {
			char *string; //< String litteral for CONST_STRING
			uint32_t string_len; //< String length for CONST_STRING
		};
		int64_t intval; //< Integer litteral for CONST_INT
		bool logicval; //< Logical litteral for CONST_LOGIC
		uint32_t arg; //< Generic argument for one argument instructions
		uint32_t argn[4]; //< Generic fixed, small number of arguments not needing malloc
		struct {
			enum ir_op op; //< operation to preform
			uint32_t lhs; //< operation left hand side
			uint32_t rhs; //< operation right hand side
		};
		struct {
			uint32_t *args; //< Generic argument list
			uint32_t arg_count; //< generic argument list length
		};
		struct {
			uint32_t *call_args; //< Arguments to function call
			uint32_t call_fun; //< Function to call
			uint32_t call_arg_count; //< Number of arguments to function call
		};
	};
	struct ir_type type; //< Resulting type of the instruction
	uint32_t out; //< Output variable or 0 for nothing
	enum ir_inst_kind kind; //< What kind of instruction
};

/// All kinds of final instructions
enum ir_term_kind {
	IR_RET, //< RET [x] (return, with result in non-void functions)
	IR_GOTO, //< GOTO b, a1, a2, ... (unconditional jump)
	IR_BRANCH, //< BRANCH c, t, f, a1, a2, ... (jump to t if true or f if false)
};
/// A final instruction, a terminator
struct ir_term {
	union {
		uint32_t ret_value; //< What to return for RET
		struct {
			uint32_t *args; //< Arguments for GOTO
			uint32_t target; //< Where to branch for GOTO
			uint32_t arg_count; //< Size of arg_count
		};
		struct {
			uint32_t *br_then_args; //< Arguments to then block
			uint32_t *br_else_args; //< Arguments to else block
			uint32_t br_cond; //< Condition to BRANCH on
			uint32_t br_then; //< Where to branch for BRANCH if true
			uint32_t br_else; //< Where to branch for BRANCH if false
			uint32_t br_then_argc; //< Size of br_then_args
			uint32_t br_else_argc; //< Size of br_else_args
		};
	};
	enum ir_term_kind kind; //< What kind of terminator
};

/// A basic block in the control flow graph
struct ir_bb {
	uint32_t *args; //< Argument variable IDs
	struct ir_type *arg_types; //< Argument types
	struct ir_inst *insts; //< Instructions
	uint32_t *preds; //< Predecessor basic blocks or NULL if reverse CFG unknown
	struct ir_term term; //< Final instruction
	uint32_t inst_count; //< Number of instructions
	uint32_t inst_back; //< Number of instructions before realloc
	uint32_t arg_count; //< Number of arguments
	uint32_t pred_count; //< Number of predecessors
};

/// A function
/// Arguments take up the first several SSA variables (starting from 1)
/// Either all basic blocks have known predecessors or none do (and all .preds are NULL)
struct ir_fun {
	char *sym; //< The symbol name to write out
	struct ir_loc loc; //< Where this funciton is from
	struct ir_type type; //< Return type
	struct ir_type *args; //< Argument types (terminated with IR_VOID) or NULL for implicit IR_VOID
	struct ir_bb *bb; //< All basic blocks, entry is bb[0]
	uint32_t arg_count; //< Number of arguments (excluding any terminators)
	uint32_t bb_count; //< Total number of basic blocks
	uint32_t bb_back; //< Backing size of bb
};

/// All functions in the program including entry points
extern struct ir_fun *ir_funs;
/// The number of functions in the program
extern size_t ir_fun_count;
/// The backing size of ir_funs
extern size_t ir_fun_back;

/// All the entry points of the program by name
extern struct ir_entry {
	char *name; //< The name of the entry point
	size_t fun; //< The function to execute (pointer into ir_funs)
} *ir_entries;
/// The number of entry points
extern size_t ir_entry_count;
/// The backing size of ir_entries
extern size_t ir_entry_back;

/// Ensure predecessor knowledge of a function
void ir_calc_fun_preds(struct ir_fun *fun);
/// Ensure predecessor knowledge of entire IR
void ir_calc_preds(void);

/// Intern big constant instructions like CONST_STRING
void ir_intern(void);

#endif
