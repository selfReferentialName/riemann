#include "util/array.h"
#include "util/xmalloc.h"
#include "util/log.h"

#include <string.h>

void array_insert(void **arr, size_t *size, size_t *back, const void *elem, size_t elemsize)
{
	if (*back <= *size) {
		if (*back == 0) {
			*back = 16;
		} else {
			*back *= 2;
		}
		*arr = xrealloc(*arr, *back * elemsize);
	}

	memcpy((char *) *arr + *size * elemsize, elem, elemsize);

	(*size)++;
}

void array_insert32(void **arr, uint32_t *size, uint32_t *back, const void *elem, size_t elemsize)
{
	if (*back <= *size) {
		if (*back == 0) {
			*back = 16;
		} else {
			*back *= 2;
		}
//		log_f(LOG_VERBOSE, "Reallocating %p to %u", arr, *back);
		*arr = xrealloc(*arr, (*back) * elemsize);
	}

//	log_f(LOG_VERBOSE, "Inserting at %u back %u elems %u bytes -- %p + %u", *size, *back, elemsize, *arr, (*size) * elemsize);
	memcpy((char *) *arr + (*size) * elemsize, elem, elemsize);

	(*size)++;
}
