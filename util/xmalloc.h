/** @file util/xmalloc.h
 * @brief Error checking malloc.
 */
#ifndef UTIL_XMALLOC_H
#define UTIL_XMALLOC_H

#include <stddef.h>

/// Call malloc and assert that it doesn't return null.
void *xmalloc(size_t size);

/// Call calloc and assert that it doesn't return null.
void *xcalloc(size_t size, size_t num);

/// Call realloc and assert that it doesn't return null unless freeing.
void *xrealloc(void *ptr, size_t size);

#endif
