#ifndef UTIL_ESCAPE_H
#define UTIL_ESCAPE_H

#include <stddef.h>

/** Escape a string according to LLVM rules.
 *
 * May change the result string up to the backing size on failure.
 *
 * @param s The string to escape.
 * @param r Where to put the result (or NULL).
 * @param n The backing size of r.
 *
 * @return 0 on success and the number of bytes needed if n is too small.
 */
size_t escape(const char *restrict s, char *restrict r, size_t n);

#endif
