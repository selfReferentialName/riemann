/** @file util/array.h
 * @brief Dynamic array handling utilities.
 */
#ifndef UTIL_ARRAY_H
#define UTIL_ARRAY_H

#include <stddef.h>
#include <stdint.h>

/** @brief Insert an item into a dynamic array.
 *
 * The dynamic array must be handled by malloc. It can be NULL, in which case
 * it will be allocated.
 *
 * When full, the array's size is doubled.
 *
 * Inserts at position initially held in size.
 *
 * @param arr A pointer to the array (may be changed).
 * @param size A pointer to the number of items in the array (always incremented).
 * @param back A pointer to the number of backing elements in the array (may be changed).
 * @param elem The item to add.
 * @param elemsize The size of elements.
 */
void array_insert(void **arr, size_t *size, size_t *back, const void *elem, size_t elemsize);

/// Like array_insert but 32-bit sizes instead
void array_insert32(void **arr, uint32_t *size, uint32_t *back, const void *elem, size_t elemsize);

#endif
