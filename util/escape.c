#include "util/escape.h"
#include <string.h>

static const char hex[0xf + 1] = {
	'0', '1', '2', '3',
	'4', '5', '6', '7',
	'8', '9', 'a', 'b',
	'c', 'd', 'e', 'f'
};

size_t escape(const char *restrict s, char *restrict r, size_t have)
{
	size_t need = 0;

	while (*s != 0) {
		if (*s < ' ' || *s > '~') {
			need += 3;
			if (have >= need) {
				*(r++) = '\\';
				*(r++) = hex[(*s & 0xf0) >> 4];
				*(r++) = hex[*s & 0x0f];
			}
		} else {
			need++;
			if (have >= need) *(r++) = *s;
		}
		s++;
	}

	need++;
	if (have >= need) *r = 0;

	if (have < need) {
		return need;
	} else {
		return 0;
	}
}
