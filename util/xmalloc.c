#include "util/xmalloc.h"
#include "util/log.h"

void *xmalloc(size_t size)
{
	void *malloc_result = malloc(size); // long name for error messages
	log_assert(malloc_result != NULL);
	return malloc_result;
}

void *xcalloc(size_t size, size_t num)
{
	void *calloc_result = calloc(size, num); // long name for error messages
	log_assert(calloc_result != NULL);
	return calloc_result;
}

void *xrealloc(void *ptr, size_t size)
{
	if (size) {
		void *realloc_result = realloc(ptr, size);
		log_assert(realloc_result != NULL);
		return realloc_result;
	} else {
		free(ptr);
		return NULL;
	}
}
