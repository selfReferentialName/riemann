#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

// OUTPUT to a file
void output(FILE *file, const char *fmt, ...);
// OUTPUT to a string
void output_string(char **result, size_t *res_len, const char *fmt, ...);

// something we can print a string to
struct buf {
	void (*write)(void *data, const char *str, size_t len);
	void *data;
};
static void write_file(void *data, const char *str, size_t len)
{
	assert(fwrite(str, 1, len, data) == len);
}
struct string {
	char *dat;
	size_t len;
	size_t back;
};
static void write_string(void *data, const char *str, size_t len)
{
	struct string *s = data;
	if (s->len + len >= s->back) {
		s->back *= 2;
		s->dat = realloc(s->dat, s->back);
	}
	memcpy(s->dat + s->len, str, len);
	s->len += len;
}

void output_buf(struct buf buf, const char *fmt, va_list ap)
{
	const char *s;
	char b[256];
	int64_t d;
	bool l;

	while (*fmt) {
		size_t i, j;
		for (i = 0; fmt[i] && fmt[i] != '%'; i++);
		if (i) buf.write(buf.data, fmt, i);
		fmt += i;
		if (*fmt == 0) return;
		fmt++;
		switch (*fmt) {
		case 's':
			s = va_arg(ap, const char *);
			buf.write(buf.data, s, strlen(s));
			break;
		case 'd':
			d = va_arg(ap, int64_t);
			i = 0;
			if (d < 0) {
				b[i] = '-';
				i++;
				d = -d;
			} else if (d == 0) {
				b[i] = '0';
				i++;
			}
			j = i;
			for (; d > 0; i++) {
				b[i] = d % 10 + '0';
				d /= 10;
			}
			for (size_t k = 0; k < (i-j)/2; k++) {
				char c = b[k+j];
				b[k+j] = b[i-k-1];
				b[i-k-1] = c;
			}
			buf.write(buf.data, b, i);
			break;
		case 'l':
			l = va_arg(ap, int); // promoted from bool
			buf.write(buf.data, l ? "TRUE" : "FALSE", l ? 4 : 5);
			break;
		}
		fmt++;
	}
}

void output(FILE *file, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	struct buf buf = { .write = write_file, .data = file };
	output_buf(buf, fmt, ap);
	va_end(ap);
}

void output_stdout(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	struct buf buf = { .write = write_file, .data = stdout };
	output_buf(buf, fmt, ap);
	va_end(ap);
}

void output_string(char **result, size_t *res_len, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	struct string str = {
		.dat = malloc(16),
		.back = 16
	};
	struct buf buf = { .write = write_string, .data = &str };
	output_buf(buf, fmt, ap);
	*result = realloc(str.dat, str.len);
	*res_len = str.len;
	va_end(ap);
}
