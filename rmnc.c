#include "frontend/frontend.h"
#include "compiler/compiler.h"
#include "util/log.h"
#include "core/disasm.h"
#include "cl.h"

int main(int argc, char **argv)
{
	const char *print_ir = NULL;
	const char *infile;
	const char *outfile;
	struct cl_interface_desc clif = {
		.program_name = "rmnc",
		.opts = (struct cl_opt[]) {
			{
				.long_name = "print-ir",
				.description = "Print post-optimisation IR",
				.type = CL_FLAG,
				.storage = &print_ir
			}
		},
		.num_opts = 1,
		.positional_args = (struct cl_arg[]) {
			{
				.name = "input",
				.description = "Input filename",
				.storage = &infile
			},
			{
				.name = "output",
				.description = "Output filename",
				.storage = &outfile
			}
		},
		.num_positional_args = 2
	};
	cl_parse(argc, argv, &clif);

	frontend(infile);
	if (print_ir) {
		ir_intern();
		core_disasm(stdout);
	}
	compile(outfile, CG_TARGET_LLVM, infile);
	return 0;
}
