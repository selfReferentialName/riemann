PROGRAM meaningless
	USE some_module ! *mildly* considering IMPORT some_module

	n = 123    :: INTEGER ! or INT
	m = 456    :: INTEGER
	x = 1.2    :: REAL
	z = 1 - 2i :: COMPLEX
	w = 0      :: COMPLEX
	p = .true. :: LOGICAL ! or LOGIC
	s = "word" :: TEXT
	c = 'h'    :: CHARACTER ! or CHAR
	v = [1, 2] :: DIMENSION(REAL, 2) ! or DIM(REAL, 2), consider REAL DIM(2) and DIM(2){ REAL }
	l = [3, 4] :: DIMENSION(REAL, :) ! or DIM(REAL, :), consider LIST(REAL)
	u = [1, 2] :: DIMENSION(REAL, 0:1)
	a          :: foo

	VARIABLE i = 1 ! or VAR i = 1, HM infered variable type, consider i = 1 :: (more consistant, looks bad)

	x = 3.4 ! assignment
	n = n + 1 ! no += operator (unless that gets annoying)
	s = "Hello, " & "world!" ! string concatenation
	n = USE x AS INTEGER ! casting, will probably be semi-implicit
	n = 7 .mod. 4 ! remainder of QRT, always positive

	p = n == m ! equality, also considering .is.
	p = n =\= m ! not equality, not sure this is the best, also consider =/=
	p = p .and. .true. ! logical and
	p = p .or. .false. ! logical or
	p = p .xor. .true. ! logical exclusive or

	x = v[2] ! array indexing, one based by default
	x = u[0] ! zero indexed array

	x = a%square(x) ! method invocation

	INPUT s ! read s as a full line
	INPUT n, m ! read two integers separated by space and/or commas with optional spacing
	INPUT("%d,%s,%d") n, s, m ! read an integer with optional space, then a comma, then a string in full, then another integer

	OUTPUT s ! output s as a full line
	OUTPUT n, s, m ! output n, a space, s, a space, and m
	OUTPUT("%d,%s,%d") n, s, m ! output n, a comma, s, a comma, and m

	WHILE n < 2
		n = n + 1
	END WHILE

	FOR n = 1:100
		w = w*w + z
		IF abs(w) > 2
			BREAK
		ELSE
			CONTINUE
		END IF
	END FOR
END PROGRAM meaningless ! everything past the END keyword is optional

! probably won't allow class definitions here
STRUCTURE foo ! class definition
	n = 1 :: PUBLIC INTEGER ! attributes can go anywhere around type
	m = 2 :: PRIVATE INTEGER
	pi = 3.14 :: PUBLIC CONSTANT REAL ! and be in any order

	SUBROUTINE bar(a, b, c)
		a :: INPUT INTEGER ! or IN INTEGER
		b :: INOUT INTEGER ! consider allowing INPUT OUTPUT INTEGER
		c :: OUTPUT INTEGER ! or OUT INTEGER

		c = a + b
		b = b * c
	END SUBROUTINE bar

	FUNCTION square(x)
		x :: REAL ! INPUT is implicit, might make it mandatory
		square :: REAL ! OUTPUT is implicit and mandatory

		RETURN x * x ! or square = x * x, which doesn't break control flow
	END FUNCTION square
END STRUCTURE foo

STRUCTURE bar INHERITS foo ! subclass definition
	SUBROUTINE bar(a, b, c)
		a :: INPUT INTEGER ! or IN INTEGER
		b :: INOUT INTEGER ! consider allowing INPUT OUTPUT INTEGER
		c :: OUTPUT INTEGER ! or OUT INTEGER

		c = a - b
		b = b / c
	END SUBROUTINE bar
END STRUCTURE bar
