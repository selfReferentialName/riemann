/* Declarative command-line parsing library.
 * This is free and unencumbered software released into the public domain.
 * Originally written by Jean-Michel Gorius -- github.com/jmgorius/cl
 */
#ifndef INCLUDED_CL_H
#define INCLUDED_CL_H

#include <stddef.h>

#ifndef CL_HELP_ALIGNMENT
#define CL_HELP_ALIGNMENT 24
#endif

/// Whether or not an option takes an argument
typedef enum cl_type { CL_FLAG, CL_VALUE } cl_type;

/// An option
typedef struct cl_opt {
  const char *short_name; //< Single character option version
  const char *long_name; //< Many character option version
  const char *description; //< Description for help printing
  const char *value_name; //< Name of argument for help printing
  const char **storage; //< Where the value is put
  cl_type type;
} cl_opt;

/// A (required) positional argument
typedef struct cl_arg {
  const char *name; //< Argument name for help printing
  const char *description; //< Argument description for help printing
  const char **storage; //< Where the value is put
} cl_arg;

/// The interface for argument parsing
typedef struct cl_interface_desc {
  const char *program_name; //< Name of the program for help messages
  const cl_opt *opts; //< Array of options
  const cl_arg *positional_args; //< Array of arguments
  const char *help_header; //< Optional thing to put at top of help
  const char *help_footer; //< Optional thing to put at bottom of help
  size_t num_opts; //< Number of options
  size_t num_positional_args; //< Number of arguments
} cl_interface_desc;

/// Parse the command line
void cl_parse(int argc, char **restrict argv, const cl_interface_desc *desc);

/// Print a help message
void cl_print_help(const cl_interface_desc *desc);

#endif /* INCLUDED_CL_H */
